<?php

class Client extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(type="integer", length=11, nullable=false)
     */
    public $id;

    /**
     *
     * @var string
     * @Column(type="string", length=300, nullable=false)
     */
    public $name;

    /**
     *
     * @var integer
     * @Column(type="integer", length=11, nullable=false)
     */
    public $status;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $created;

    /**
     *
     * @var string
     * @Column(type="string", nullable=false)
     */
    public $updated;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("pledge_ms");
        $this->setSource("client");
        $this->hasMany('id', 'ClientSetting', 'client_id', ['alias' => 'ClientSetting']);
        $this->hasMany('id', 'Notification', 'client_id', ['alias' => 'Notification']);
        $this->hasMany('id', 'NotificationGroup', 'client_id', ['alias' => 'NotificationGroup']);
        $this->hasMany('id', 'Outbox', 'client_id', ['alias' => 'Outbox']);
        $this->hasMany('id', 'Payment', 'client_id', ['alias' => 'Payment']);
        $this->hasMany('id', 'Pledge', 'client_id', ['alias' => 'Pledge']);
        $this->hasMany('id', 'ProfileSetting', 'client_id', ['alias' => 'ProfileSetting']);
        $this->hasMany('id', 'Transaction', 'client_id', ['alias' => 'Transaction']);
        $this->hasMany('id', 'UserRole', 'client_id', ['alias' => 'UserRole']);
        $this->hasMany('id', 'UserSetting', 'client_id', ['alias' => 'UserSetting']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'client';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Client[]|Client|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Client|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
