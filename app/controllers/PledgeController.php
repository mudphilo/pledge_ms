<?php
	use Phalcon\Http\Request;


class PledgeController extends ControllerBase
{
    public function indexAction()
    {

    }

	public function create()
	{
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$msisdn     = isset($json->msisdn) ? $json->msisdn : false;
		$amount  	= isset($json->amount) ? $json->amount : false;
		$redeem_date = isset($json->redeem_date) ? $json->redeem_date : '2018-05-30';
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id || !$redeem_date)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}


		$msisdn = $this->formatMobileNumber($msisdn);

		if (!$msisdn)
		{
			return $this->missingData();
		}

		$profile = $this->createProfile($msisdn);

		if(!$amount){
			return $this->missingData();
		}

		$pledge = Pledge::findFirst(array("profile_id=:profile_id:",'bind' => array("profile_id" => $profile->id)));

		if(!$pledge){

			$pledge = new Pledge();
			$pledge->profile_id = $profile->id;
			$pledge->client_id = 1;
			$pledge->status = 0;
			$pledge->created = $this->getTime();
		}


		$pledge->amount = $amount;
		$pledge->redeem_date = $redeem_date;

		if ($pledge->save() === false)
		{
			$errors   = array();
			$messages = $pledge->getMessages();
			foreach ($messages as $message)
			{
				$e["message"] = $message->getMessage();
				$e["field"]   = $message->getField();
				$errors[]     = $e;
			}
			return $this->systemResponse($errors,421,"failed to created pledge");
		}


		return $this->systemResponse($pledge,200,'Pledge Recorded Successfully');
	}

	/**
	 * updates pledge a user
	 *
	 * @param int $id
	 */
	public function update($id)
	{
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$amount     = isset($json->amount) ? $json->amount : false;
		$redeem_date     = isset($json->redeem_date) ? $json->redeem_date : false;
		$status      = isset($json->status) ? $json->status : false;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$pledge = Pledge::findFirst(array("id=:id:",'bind' => array("id" => $id)));
		if (!$pledge)
		{
			return $this->missingData("Pledge not found");
		}

		if($status){
			$pledge->status = $status;
		}

		if($amount){
			$pledge->amount = $amount;
		}

		if($redeem_date){
			$pledge->redeem_date = $redeem_date;
		}

		if ($pledge->save() === false)
		{
			$errors   = array();

			$messages = $pledge->getMessages();
			foreach ($messages as $message)
			{
				$e["message"] = $message->getMessage();
				$e["field"]   = $message->getField();
				$errors[]     = $e;
			}
			return $this->systemResponse($errors,421,"FAILED");
		}

		return $this->systemResponse("Pledge Updated");
	}

	/**
	 * gets pledge vue-table
	 */
	public function table()
	{
		$this->view->disable();
		$request    = new Request();
		$json       = $request->getJsonRawBody();

		$sort       = isset($json->sort) ? $json->sort : false;
		$per_page   = isset($json->per_page) ? $json->per_page : false;
		$page       = isset($json->page) ? $json->page : false;
		$filter_raw = isset($json->filter) ? $json->filter : false;
		$start      = isset($json->start) ? $json->start : false;
		$end        = isset($json->end) ? $json->end : false;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$filter_raw = trim($filter_raw);

		if($filter_raw == 'undefined')
		{
			$filter_raw = false;
		}

		$filter     = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
		$start = (isset($start) && $start != 'null') ? $start : false;
		$end = (isset($end) && $end != 'null') ? $end : false;

		$extraWhere = array();

		$table = "pledge";

		$primaryKey = "id";

		if ($start && $end)
		{
			$extraWhere[] = "DATE(pledge.created) >= '$start' AND DATE(pledge.created) <= '$end' ";
		}

		if ($filter)
		{
			if (strlen($filter) > 3)
			{
				$extraWhere[] = "profile.msisdn REGEXP '$filter' ";
			}
		}

		$extraWhere[] = "pledge.client_id = 1 ";
		//$extraWhere[] = "payment.status = 0 ";

		if (count($extraWhere) > 0)
		{
			$where = implode(" AND ",$extraWhere);
		}
		else
		{
			$where = 1;
		}

		$joinQuery [] = "LEFT JOIN pledge_payment On pledge.id = pledge_payment.pledge_id ";
		$joinQuery [] = "LEFT JOIN payment On pledge_payment.payment_id = payment.id ";
		$joinQuery [] = "LEFT JOIN profile On pledge.profile_id = profile.id ";

		$fields [] = "$table.$primaryKey";
		$fields [] = "profile.msisdn";
		$fields [] = "profile.first_name";
		$fields [] = "profile.middle_name";
		$fields [] = "profile.last_name";
		$fields [] = "profile.status";
		$fields [] = "DATE_FORMAT(pledge.created,'%h:%i%, %d %b %y') as created";
		$fields [] = "DATE_FORMAT(pledge.redeem_date,'%h:%i%, %d %b %y') as redeem_date";
		$fields [] = "SUM(payment.amount) as payments";
		$fields [] = "payment.status as paymentStatus";
		$fields [] = "pledge.amount as pledge";


		$groupBy = array();
		$groupBy[] = "pledge.id";

		if (count($joinQuery) > 0)
		{
			$join = implode(" ",$joinQuery);
		}
		else
		{
			$join = '';
		}

		if (count($fields) > 0)
		{
			$fields = implode(",",$fields);
		}
		else
		{
			$fields = " $table.$primaryKey ";
		}

		if($sort)
		{
			list($sortByColumn,$sortBy) = explode('|',$sort);
			$orderBy = "ORDER BY $sortByColumn $sortBy";
		}
		else
		{
			$orderBy = "";
		}

		if (count($groupBy) > 0)
		{
			$group_by = "GROUP BY ".implode(" ",$groupBy);
		}
		else
		{
			$group_by = '';
		}

		$export        = $request->getQuery('export');

		$export = isset($export) ? $export : 0;

		if($export == 1)
		{
			$sql = "SELECT $fields "
				. "FROM $table $join "
				. "WHERE $where "
				. "$orderBy ";

			return $this->exportQuery($sql);
		}

		$countQuery = "SELECT COUNT(DISTINCT $table.$primaryKey) id FROM `$table` $join WHERE $where ";

		try
		{
			$total = $this->rawSelect($countQuery);
		}
		catch (Exception $e)
		{
			$this->log("error", $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		$total = isset($total[0]['id']) ? $total[0]['id'] : 0;

		$last_page = $this->calculateTotalPages($total,$per_page);

		$current_page = $page - 1;

		if ($current_page)
		{

			$offset = $per_page * $current_page;
		}
		else
		{
			$current_page = 0;
			$offset       = 0;
		}

		if ($offset > $total)
		{

			$offset = $total - ($current_page * $per_page);
		}

		$from = $offset + 1;

		$current_page++;

		$left_records = $total - ($current_page * $per_page);

		$sql = "SELECT $fields "
			. "FROM $table $join "
			. "WHERE $where "
			. " $group_by "
			. "$orderBy "
			. "LIMIT $offset,$per_page";

		$next_page_url = $left_records > 0 ? "api/v1/profile/table" : null;

		$prev_page_url = ($left_records + $per_page) < $total ? "api/v1/profile/table" : null;

		try
		{
			$transactions = $this->rawSelect($sql);
		}
		catch (Exception $e)
		{
			$this->log("error, " . $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		if ($transactions)
		{
			$tableData['total']         = $total;
			$tableData['per_page']      = $per_page;
			$tableData['next_page_url'] = $next_page_url;
			$tableData['prev_page_url'] = $prev_page_url;
			$tableData['current_page']  = $current_page;
			$tableData['last_page']     = $last_page;
			$tableData['from']          = $from;
			$tableData['to']            = $offset + count($transactions);

			$tableData['data'] = $transactions;

			return $this->systemResponse($tableData,200,"Success");
		}
		else
		{
			$tableData['data'] = [];
			return $this->systemResponse($tableData,200,"Not Found");
		}

		return $this->systemResponse($tableData,421,'Not Found');
	}

	/**
	 * @param $id
	 *
	 */
	public function pay($id)
	{
		$this->view->disable();
		$request    		= new Request();
		$json       		= $request->getJsonRawBody();
		$amount     		= isset($json->amount) ? $json->amount : false;
		$payment_type_id   	= isset($json->payment_type_id) ? $json->payment_type_id : false;
		$reference         	= isset($json->reference) ? $json->reference : "NO_REFERENCE";
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		if (!$amount || !$payment_type_id)
		{
			return $this->missingData();
		}

		$pledge = Pledge::findFirst(array("id=:id:",'bind' => array("id" => $id)));

		if (!$pledge)
		{
			return $this->missingData("Pledge Not Found");
		}

		$pledgePayment = PledgePayment::findFirst(array("pledge_id=:pledge_id:",'bind' => array("pledge_id" => $id)));

		if($pledgePayment){

			$payment_id = $pledgePayment->payment_id;

			$payment = Payment::findFirst(array("id=:id:",'bind' => array("id" => $payment_id)));

			if($payment){
				$payment->amount = $amount;
				$payment->save();
				return $this->systemResponse("Pledge payment recorded");
			}
		}

		$payment = new Payment();
		$payment->reference = $reference;
		$payment->amount = $amount;
		$payment->status = 1;
		$payment->client_id = 1;
		$payment->payment_type_id = $payment_type_id;
		$payment->created = $this->getTime();
		$payment->profile_id = $pledge->profile_id;

		if ($payment->save() === false)
		{
			$errors   = array();

			$messages = $payment->getMessages();
			foreach ($messages as $message)
			{
				$e["message"] = $message->getMessage();
				$e["field"]   = $message->getField();
				$errors[]     = $e;
			}
			return $this->systemResponse($errors,421,"FAILED");
		}

		$pledgePayment = new PledgePayment();
		$pledgePayment->created = $this->getTime();
		$pledgePayment->payment_id = $payment->id;
		$pledgePayment->pledge_id = $pledge->id;

		if ($pledgePayment->save() === false)
		{
			$errors   = array();

			$messages = $pledgePayment->getMessages();
			foreach ($messages as $message)
			{
				$e["message"] = $message->getMessage();
				$e["field"]   = $message->getField();
				$errors[]     = $e;
			}
			return $this->systemResponse($errors,421,"FAILED");
		}

		return $this->systemResponse("Pledge payment recorded");
	}

	/**
	 * @param $id
	 *
	 */
	public function view($id)
	{
		$this->view->disable();
		$request    		= new Request();
		$json       		= $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$pledge = Pledge::findFirst(array("id=:id:",'bind' => array("id" => $id)));

		if (!$pledge)
		{
			return $this->missingData("Pledge Not Found");
		}

		$payments = array();

		foreach ($pledge->PledgePayment as $pledgePayment){

			$payments[] = $pledgePayment->Payment;
		};

		return $this->systemResponse($payments,200,"Pledge payment retrieved");
	}

	/**
	 * gets payment vue-table
	 */
	public function summary()
	{
		$this->view->disable();
		$request    = new Request();
		$json       = $request->getJsonRawBody();

		$filter_raw = isset($json->filter) ? $json->filter : false;
		$start      = isset($json->start) ? $json->start : false;
		$end        = isset($json->end) ? $json->end : false;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$filter_raw = trim($filter_raw);

		if($filter_raw == 'undefined')
		{
			$filter_raw = false;
		}

		$filter     = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
		$start = (isset($start) && $start != 'null') ? $start : false;
		$end = (isset($end) && $end != 'null') ? $end : false;

		$extraWhere = array();

		$table = "pledge";

		$primaryKey = "id";

		if ($start && $end)
		{
			$extraWhere[] = "DATE(pledge.created) >= '$start' AND DATE(pledge.created) <= '$end' ";
		}

		if ($filter)
		{
			if (strlen($filter) > 3)
			{
				$extraWhere[] = "profile.msisdn REGEXP '$filter' ";
			}
		}

		$extraWhere[] = "pledge.client_id = 1 ";
		//$extraWhere[] = "payment.status = 0 ";

		if (count($extraWhere) > 0)
		{
			$where = implode(" AND ",$extraWhere);
		}
		else
		{
			$where = 1;
		}

		$joinQuery [] = "LEFT JOIN pledge_payment On pledge.id = pledge_payment.pledge_id ";
		$joinQuery [] = "LEFT JOIN payment On pledge_payment.payment_id = payment.id ";
		$joinQuery [] = "LEFT JOIN profile On pledge.profile_id = profile.id ";

		$fields [] = "SUM(payment.amount) as redeemed";
		$fields [] = "SUM(pledge.amount) as pledged";


		$groupBy = array();

		if (count($joinQuery) > 0)
		{
			$join = implode(" ",$joinQuery);
		}
		else
		{
			$join = '';
		}

		if (count($fields) > 0)
		{
			$fields = implode(",",$fields);
		}
		else
		{
			$fields = " $table.$primaryKey ";
		}

		if (count($groupBy) > 0)
		{
			$group_by = "GROUP BY ".implode(" ",$groupBy);
		}
		else
		{
			$group_by = '';
		}

		$sql = "SELECT $fields "
			. "FROM $table $join "
			. "WHERE $where "
			. " $group_by ";

		try
		{
			$total = $this->rawSelect($sql);
			$res = array();

			foreach ($total as $row){
				$res = array();
				$res[] = array("name"=>"Redeemed","total"=>$row['redeemed']);
				$res[] = array("name"=>"Pledged","total"=>$row['pledged']);
				$res[] = array("name"=>"Balance","total"=>($row['pledged'] - $row['redeemed']));

			}
			return $this->systemResponse($res,200,"Success");
		}
		catch (Exception $e)
		{
			$this->log("error", $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}
	}

}

