<?php

	use Phalcon\Http\Request;

class GroupController extends ControllerBase
{

	/**
	 * created a group resource
	 */
    public function create()
    {
        $request      = new Request();
        $json         = $request->getJsonRawBody();
        $name       = $json->name;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}


		if (!$name)
        {
            return $this->missingData();
        }

        $filters = ["name=:name: AND client_id = :client_id: ","bind"=>["name"=>$name,"client_id"=>1]];

		$group = NotificationGroup::findFirst($filters);
		if ($group)
		{
			return $this->missingData("Group with similar name exists");
		}

        $group               = new NotificationGroup();
        $group->created    	= $this->getTime();
        $group->name    = $name;
        $group->client_id = 1;

        if ($group->save() === false)
        {
            $errors   = array();
            $messages = $group->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
			return $this->systemResponse($errors,421,"failed to created group");
		}

		return $this->systemResponse("group created successfully",200,'Group Created Successfully');
    }

    public function view($id)
    {
        $request = new Request();
        $json    = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$filters = ["id=:id: ","bind"=>["id"=>$id]];

		$group = NotificationGroup::findFirst($filters);
		if (!$group)
		{
			return $this->missingData("Group does not exists");
		}

		$sql = "SELECT p.msisdn,p.first_name,p.middle_name,p.last_name FROM profile_group pg INNER JOIN profile p ON pg.profile_id=p.id INNER JOIN notification_group ng ON pg.notification_group_id=ng.id WHERE ng.id = :id";

		$params = array(":id"=>$group->id);

		$data = $this->rawSelect($sql,$params);

		$response = new stdClass();
		$response->id = $group->id;
		$response->name = $group->name;
		$response->created = $group->created;
		$response->client_id = $group->client_id;
		$response->members = $data;

		return $this->systemResponse($response,200,"SUCCESS");
    }

	public function all()
	{
		$request = new Request();
		$json    = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id) {
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
		if (!$user) {
			return $this->accessDenied();
		}

		if (!$this->isValidUser($token, $user)) {
			return $this->invalidToken();
		}

		$sql = "SELECT g.id,g.name, count(pg.id) as size from notification_group g left join profile_group pg on g.id=pg.notification_group_id group by g.id ";

		$data = $this->rawSelect($sql);

		return $this->systemResponse($data,200,"SUCCESS");
	}

	public function join($id)
	{
		$request = new Request();
		$json    = $request->getJsonRawBody();
		$user_id    = isset($json->user_id) ? $json->user_id : false;
		$msisdn     = isset($json->msisdn) ? $json->msisdn : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		if (!$msisdn)
		{
			return $this->missingData();
		}

		$filters = ["id=:id: ","bind"=>["id"=>$id]];

		$group = NotificationGroup::findFirst($filters);

		if (!$group)
		{
			return $this->missingData("Group does not exists");
		}

		$msisdns = explode(",", $msisdn);

		$data = array();

		foreach ($msisdns as $mobile){

			$number = $this->formatMobileNumber($mobile);
			if($number){
				$profile = $this->createProfile($number);
				$data[]  = "(".$profile->id.",".$group->id.",now())";
			}
		}


		$sql = "INSERT IGNORE INTO profile_group (profile_id,notification_group_id,created) VALUES ".implode(",",$data);

		$this->log("infor",$sql);

		$this->rawInsert($sql);

		return $this->systemResponse("Members added to group",200,"SUCCESS");
	}

	public function leave($id)
	{
		$request = new Request();
		$json    = $request->getJsonRawBody();
		$user_id    = isset($json->user_id) ? $json->user_id : false;
		$msisdn     = isset($json->msisdn) ? $json->msisdn : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		if (!$msisdn)
		{
			return $this->missingData();
		}

		$filters = ["id=:id: ","bind"=>["id"=>$id]];

		$group = NotificationGroup::findFirst($filters);

		if (!$group)
		{
			return $this->missingData("Group does not exists");
		}

		$msisdns = explode(",", $msisdn);

		$data = array();

		foreach ($msisdns as $mobile){

			$number = $this->formatMobileNumber($mobile);
			if($number){
				$profile = $this->createProfile($number);
				$data[]  = $profile->id;
			}
		}


		$sql = "DELETE FROM profile_group WHERE profile_id IN (".implode(",",$data).")";

		$data = $this->rawInsert($sql);

		return $this->systemResponse("Members unsubscribed to group",200,"SUCCESS");
	}

    public function update($id)
    {
        $request              = new Request();
        $json                 = $request->getJsonRawBody();
		$name     		= isset($json->name) ? $json->name : false;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id || !$name)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$group = NotificationGroup::findFirst(array("id=:id:",'bind' => array("id" => $id)));

		if(!$group){
			return $this->missingData("Group not found");
		}

		$filters = ["name=:name: AND client_id = :client_id: ","bind"=>["name"=>$name,"client_id"=>1]];

		$groupCheck = NotificationGroup::findFirst($filters);
		if ($groupCheck)
		{
			return $this->missingData("Group with similar name exists");
		}

		$group->name = $name;

        if ($group->save() === false)
        {
            $errors   = array();
            $messages = $group->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse($group,200,'Group Updated');
    }

    public function delete($id)
    {
        $request    = new Request();
        $json       = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}


		$group = NotificationGroup::findFirst(array("id=:id:",'bind' => array("id" => $id)));

		if(!$group){
			return $this->missingData("Group not found");
		}

		$sql = "DELETE FROM profile_group WHERE notification_group_id = ".$group->id;

		$this->rawInsert($sql);

        if ($group->delete() === false)
        {
            $errors   = array();
            $messages = $group->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse('Group Deleted');
    }

	/**
	 * gets payment vue-table
	 */
	public function table()
	{
		$this->view->disable();
		$request    = new Request();
		$json       = $request->getJsonRawBody();

		$sort       = isset($json->sort) ? $json->sort : false;
		$per_page   = isset($json->per_page) ? $json->per_page : false;
		$page       = isset($json->page) ? $json->page : false;
		$filter_raw = isset($json->filter) ? $json->filter : false;
		$start      = isset($json->start) ? $json->start : false;
		$end        = isset($json->end) ? $json->end : false;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$filter_raw = trim($filter_raw);

		if($filter_raw == 'undefined')
		{
			$filter_raw = false;
		}

		$filter     = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
		$start = (isset($start) && $start != 'null') ? $start : false;
		$end = (isset($end) && $end != 'null') ? $end : false;

		$extraWhere = array();

		$table = "notification_group";

		$primaryKey = "id";

		if ($start && $end)
		{
			$extraWhere[] = "DATE(notification_group.created) >= '$start' AND DATE(notification_group.created) <= '$end' ";
		}

		if ($filter)
		{
			if (strlen($filter) > 3)
			{
				$extraWhere[] = "notification_group.name REGEXP '$filter'";
			}
		}

		$extraWhere[] = "notification_group.client_id = 1 ";

		if (count($extraWhere) > 0)
		{
			$where = implode(" AND ",$extraWhere);
		}
		else
		{
			$where = 1;
		}

		$joinQuery [] = "LEFT JOIN profile_group On notification_group.id = profile_group.notification_group_id ";

		$fields [] = "$table.$primaryKey";
		$fields [] = "notification_group.name";
		$fields [] = "COUNT(profile_group.profile_id) as members";

		$groupBy = array();
		$groupBy[] = "notification_group.id";

		if (count($joinQuery) > 0)
		{
			$join = implode(" ",$joinQuery);
		}
		else
		{
			$join = '';
		}

		if (count($fields) > 0)
		{
			$fields = implode(",",$fields);
		}
		else
		{
			$fields = " $table.$primaryKey ";
		}

		if($sort)
		{
			list($sortByColumn,$sortBy) = explode('|',$sort);
			$orderBy = "ORDER BY $sortByColumn $sortBy";
		}
		else
		{
			$orderBy = "";
		}

		if (count($groupBy) > 0)
		{
			$group_by = "GROUP BY ".implode(" ",$groupBy);
		}
		else
		{
			$group_by = '';
		}

		$export        = $request->getQuery('export');

		$export = isset($export) ? $export : 0;

		if($export == 1)
		{
			$sql = "SELECT $fields "
				. "FROM $table $join "
				. "WHERE $where "
				. "$orderBy ";

			return $this->exportQuery($sql);
		}

		$countQuery = "SELECT COUNT(DISTINCT $table.$primaryKey) id FROM `$table` $join WHERE $where ";

		try
		{
			$total = $this->rawSelect($countQuery);
		}
		catch (Exception $e)
		{
			$this->log("error", $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		$total = isset($total[0]['id']) ? $total[0]['id'] : 0;

		$last_page = $this->calculateTotalPages($total,$per_page);

		$current_page = $page - 1;

		if ($current_page)
		{

			$offset = $per_page * $current_page;
		}
		else
		{
			$current_page = 0;
			$offset       = 0;
		}

		if ($offset > $total)
		{

			$offset = $total - ($current_page * $per_page);
		}

		$from = $offset + 1;

		$current_page++;

		$left_records = $total - ($current_page * $per_page);

		$sql = "SELECT $fields "
			. "FROM $table $join "
			. "WHERE $where "
			. " $group_by "
			. "$orderBy "
			. "LIMIT $offset,$per_page";

		$next_page_url = $left_records > 0 ? "api/v1/group/table" : null;

		$prev_page_url = ($left_records + $per_page) < $total ? "api/v1/group/table" : null;

		try
		{
			$transactions = $this->rawSelect($sql);
		}
		catch (Exception $e)
		{
			$this->log("error, " . $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		if ($transactions)
		{
			$tableData['total']         = $total;
			$tableData['per_page']      = $per_page;
			$tableData['next_page_url'] = $next_page_url;
			$tableData['prev_page_url'] = $prev_page_url;
			$tableData['current_page']  = $current_page;
			$tableData['last_page']     = $last_page;
			$tableData['from']          = $from;
			$tableData['to']            = $offset + count($transactions);

			$tableData['data'] = $transactions;

			return $this->systemResponse($tableData,200,"Success");
		}
		else
		{
			$tableData['data'] = [];
			return $this->systemResponse($tableData,200,"Not Found");
		}

		return $this->systemResponse($tableData,421,'Not Found');
	}

	/**
	 * gets payment vue-table
	 */
	public function members($id)
	{
		$this->view->disable();
		$request    = new Request();
		$json       = $request->getJsonRawBody();

		$sort       = isset($json->sort) ? $json->sort : false;
		$per_page   = isset($json->per_page) ? $json->per_page : false;
		$page       = isset($json->page) ? $json->page : false;
		$filter_raw = isset($json->filter) ? $json->filter : false;
		$start      = isset($json->start) ? $json->start : false;
		$end        = isset($json->end) ? $json->end : false;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$filter_raw = trim($filter_raw);

		if($filter_raw == 'undefined')
		{
			$filter_raw = false;
		}

		$filter     = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
		$start = (isset($start) && $start != 'null') ? $start : false;
		$end = (isset($end) && $end != 'null') ? $end : false;

		$extraWhere = array();

		$table = "notification_group";

		$primaryKey = "id";

		$extraWhere[] = "profile_group.notification_group_id= $id ";

		if ($filter)
		{
			if (strlen($filter) > 3)
			{
				$extraWhere[] = "profile.msisdn REGEXP '$filter' OR profile.first_name REGEXP '$filter' OR profile.middle_name REGEXP '$filter' OR profile.last_name REGEXP '$filter'";
			}
		}

		$extraWhere[] = "notification_group.client_id = 1 ";

		if (count($extraWhere) > 0)
		{
			$where = implode(" AND ",$extraWhere);
		}
		else
		{
			$where = 1;
		}

		$joinQuery [] = "INNER JOIN profile_group On notification_group.id = profile_group.notification_group_id ";
		$joinQuery [] = "LEFT JOIN profile On profile_group.profile_id = profile.id ";

		$fields [] = "$table.$primaryKey";
		$fields [] = "profile.id as profile_id";
		$fields [] = "profile.msisdn";
		$fields [] = "profile.first_name";
		$fields [] = "profile.middle_name";
		$fields [] = "profile.last_name";
		$fields [] = "profile_group.created";

		$groupBy = array();
		$groupBy[] = "profile.id";

		if (count($joinQuery) > 0)
		{
			$join = implode(" ",$joinQuery);
		}
		else
		{
			$join = '';
		}

		if (count($fields) > 0)
		{
			$fields = implode(",",$fields);
		}
		else
		{
			$fields = " $table.$primaryKey ";
		}

		if($sort)
		{
			list($sortByColumn,$sortBy) = explode('|',$sort);
			$orderBy = "ORDER BY $sortByColumn $sortBy";
		}
		else
		{
			$orderBy = "";
		}

		if (count($groupBy) > 0)
		{
			$group_by = "GROUP BY ".implode(" ",$groupBy);
		}
		else
		{
			$group_by = '';
		}

		$export        = $request->getQuery('export');

		$export = isset($export) ? $export : 0;

		if($export == 1)
		{
			$sql = "SELECT $fields "
				. "FROM $table $join "
				. "WHERE $where "
				. "$orderBy ";

			return $this->exportQuery($sql);
		}

		$countQuery = "SELECT COUNT(DISTINCT $table.$primaryKey) id FROM `$table` $join WHERE $where $group_by";

		try
		{
			$total = $this->rawSelect($countQuery);
		}
		catch (Exception $e)
		{
			$this->log("error", $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		$total = isset($total[0]['id']) ? $total[0]['id'] : 0;

		$last_page = $this->calculateTotalPages($total,$per_page);

		$current_page = $page - 1;

		if ($current_page)
		{

			$offset = $per_page * $current_page;
		}
		else
		{
			$current_page = 0;
			$offset       = 0;
		}

		if ($offset > $total)
		{

			$offset = $total - ($current_page * $per_page);
		}

		$from = $offset + 1;

		$current_page++;

		$left_records = $total - ($current_page * $per_page);

		$sql = "SELECT $fields "
			. "FROM $table $join "
			. "WHERE $where "
			. " $group_by "
			. "$orderBy "
			. "LIMIT $offset,$per_page";

		$next_page_url = $left_records > 0 ? "group/$id/members" : null;

		$prev_page_url = ($left_records + $per_page) < $total ? "group/$id/members" : null;

		try
		{
			$transactions = $this->rawSelect($sql);
		}
		catch (Exception $e)
		{
			$this->log("error, " . $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		if ($transactions)
		{
			$tableData['total']         = $total;
			$tableData['per_page']      = $per_page;
			$tableData['next_page_url'] = $next_page_url;
			$tableData['prev_page_url'] = $prev_page_url;
			$tableData['current_page']  = $current_page;
			$tableData['last_page']     = $last_page;
			$tableData['from']          = $from;
			$tableData['to']            = $offset + count($transactions);

			$tableData['data'] = $transactions;

			return $this->systemResponse($tableData,200,"Success");
		}
		else
		{
			$tableData['data'] = [];
			return $this->systemResponse($tableData,200,"Not Found");
		}

		return $this->systemResponse($tableData,421,'Not Found');
	}

}
