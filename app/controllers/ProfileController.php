<?php
	use Phalcon\Http\Request;


class ProfileController extends ControllerBase
{
    public function indexAction()
    {

    }

	public function create()
	{
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$first_name  	= isset($json->first_name) ? $json->first_name : false;
		$middle_name  	= isset($json->middle_name) ? $json->middle_name : false;
		$last_name  	= isset($json->last_name) ? $json->last_name : false;
		$msisdn     = isset($json->msisdn) ? $json->msisdn : false;
		$email  	= isset($json->email) ? $json->email : false;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}


		$msisdn = $this->formatMobileNumber($msisdn);

		if (!$msisdn)
		{
			return $this->missingData("invalid MSIDN supplied $msisdn");
		}

		$profile = $this->profileExists($msisdn);

		if (!$profile)
		{
			$profile = new Profile();
			$profile->msisdn = $msisdn;
			$profile->status = 1;
			$profile->created = $this->getTime();
		}

		if($middle_name){
			$profile->middle_name = $middle_name;
		}

		if($first_name){
			$profile->first_name = $first_name;
		}

		if($last_name){
			$profile->last_name = $last_name;
		}

		if($email){
			$profile->email = $email;
		}

		if ($profile->save() === false)
		{
			$errors   = array();
			$messages = $profile->getMessages();
			foreach ($messages as $message)
			{
				$e["message"] = $message->getMessage();
				$e["field"]   = $message->getField();
				$errors[]     = $e;
			}
			return $this->systemResponse($errors,421,"profile register failed");
		}

		$profileSetting = ProfileSetting::findFirst(
				array(
					"profile_id=:profile_id:",
					'bind' => array(
						"profile_id" => $profile->id)
				));

		if(!$profileSetting){

			$profileSetting = new ProfileSetting();
			$profileSetting->profile_id 	= $profile->id;
			$profileSetting->client_id	 	= 1;
			$profileSetting->status			= 1;
			$profileSetting->created   = $this->getTime();

			if ($profileSetting->save() === false)
			{
				$errors   = array();
				$messages = $profileSetting->getMessages();
				foreach ($messages as $message)
				{
					$e["message"] = $message->getMessage();
					$e["field"]   = $message->getField();
					$errors[]     = $e;
				}
				return $this->systemResponse($errors,421,"Could not map profile to client for the newly created user");
			}

		}

		return $this->systemResponse($profile,200,'Verification Code Send');
	}

	/**
	 * @param $msisdn
	 *
	 * @return bool|\Phalcon\Mvc\Model\ResultInterface|\Profile
	 */

	public function profileExists($msisdn)
	{
		$profile = Profile::findFirst(array("msisdn=:username:",'bind' => array("username" => $msisdn)));

		if($profile){

			return $profile;

		}

		return false;
	}

	public function table()
	{
		$this->view->disable();
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$sort       = isset($json->sort) ? $json->sort : false;
		$per_page   = isset($json->per_page) ? $json->per_page : false;
		$page       = isset($json->page) ? $json->page : false;
		$filter_raw = isset($json->filter) ? $json->filter : false;
		$start      = isset($json->start) ? $json->start : false;
		$end        = isset($json->end) ? $json->end : false;

		$filter_raw = trim($filter_raw);

		if($filter_raw == 'undefined')
		{
			$filter_raw = false;
		}

		$filter     = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
		$start = (isset($start) && $start != 'null') ? $start : false;
		$end = (isset($end) && $end != 'null') ? $end : false;

		$extraWhere = array();

		$table = "profile";

		$primaryKey = "id";

		if ($start && $end)
		{
			$extraWhere[] = "DATE(profile.created) >= '$start' AND DATE(profile.created) <= '$end' ";
		}

		if ($filter)
		{
			if (strlen($filter) > 3)
			{
				$extraWhere[] = "profile.msisdn REGEXP '$filter' OR profile.first_name REGEXP '$filter' OR profile.middle_name REGEXP '$filter' OR profile.last_name REGEXP '$filter' ";
			}
		}

		$extraWhere[] = "profile_setting.client_id = 1 ";
		//$extraWhere[] = "payment.status = 1 ";

		if (count($extraWhere) > 0)
		{
			$where = implode(" AND ",$extraWhere);
		}
		else
		{
			$where = 1;
		}

		$joinQuery [] = "INNER JOIN profile_setting On profile.id = profile_setting.profile_id ";
		$joinQuery [] = "LEFT JOIN payment On profile.id = payment.profile_id AND payment.client_id = 1";
		$joinQuery [] = "LEFT JOIN pledge On profile.id = pledge.profile_id AND pledge.client_id = 1";

		$fields [] = "$table.$primaryKey";
		$fields [] = "profile.msisdn";
		$fields [] = "profile.first_name";
		$fields [] = "profile.middle_name";
		$fields [] = "profile.last_name";
		$fields [] = "profile.status";
		$fields [] = "DATE_FORMAT(profile.created,'%h:%i%, %d %b %y') as created";
		$fields [] = "SUM(payment.amount) as payments";
		$fields [] = "SUM(pledge.amount) as pledges";

		$groupBy = array();
		$groupBy[] = "profile.id";

		if (count($joinQuery) > 0)
		{
			$join = implode(" ",$joinQuery);
		}
		else
		{
			$join = '';
		}

		if (count($fields) > 0)
		{
			$fields = implode(",",$fields);
		}
		else
		{
			$fields = " $table.$primaryKey ";
		}

		if($sort)
		{
			list($sortByColumn,$sortBy) = explode('|',$sort);
			$orderBy = "ORDER BY $sortByColumn $sortBy";
		}
		else
		{
			$orderBy = "";
		}

		if (count($groupBy) > 0)
		{
			$group_by = "GROUP BY ".implode(" ",$groupBy);
		}
		else
		{
			$group_by = '';
		}

		$export        = $request->getQuery('export');

		$export = isset($export) ? $export : 0;

		if($export == 1)
		{
			$sql = "SELECT $fields "
				. "FROM $table $join "
				. "WHERE $where "
				. "$orderBy ";

			return $this->exportQuery($sql);
		}

		$countQuery = "SELECT COUNT(DISTINCT $table.$primaryKey) id FROM `$table` $join WHERE $where ";

		try
		{
			$total = $this->rawSelect($countQuery);
		}
		catch (Exception $e)
		{
			$this->log("error", $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		$total = isset($total[0]['id']) ? $total[0]['id'] : 0;

		$last_page = $this->calculateTotalPages($total,$per_page);

		$current_page = $page - 1;

		if ($current_page)
		{

			$offset = $per_page * $current_page;
		}
		else
		{
			$current_page = 0;
			$offset       = 0;
		}

		if ($offset > $total)
		{

			$offset = $total - ($current_page * $per_page);
		}

		$from = $offset + 1;

		$current_page++;

		$left_records = $total - ($current_page * $per_page);

		$sql = "SELECT $fields "
			. "FROM $table $join "
			. "WHERE $where "
			. " $group_by "
			. "$orderBy "
			. "LIMIT $offset,$per_page";

		$next_page_url = $left_records > 0 ? "api/v1/profile/table" : null;

		$prev_page_url = ($left_records + $per_page) < $total ? "api/v1/profile/table" : null;

		try
		{
			$transactions = $this->rawSelect($sql);
		}
		catch (Exception $e)
		{
			$this->log("error, " . $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		if ($transactions)
		{
			$tableData['total']         = $total;
			$tableData['per_page']      = $per_page;
			$tableData['next_page_url'] = $next_page_url;
			$tableData['prev_page_url'] = $prev_page_url;
			$tableData['current_page']  = $current_page;
			$tableData['last_page']     = $last_page;
			$tableData['from']          = $from;
			$tableData['to']            = $offset + count($transactions);

			$tableData['data'] = $transactions;

			return $this->systemResponse($tableData,200,"Success");
		}
		else
		{
			$tableData['data'] = [];
			return $this->systemResponse($tableData,200,"Not Found");
		}

		return $this->systemResponse($tableData,421,'Not Found');
	}

	public function payment($id)
	{
		$this->view->disable();
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$sort       = isset($json->sort) ? $json->sort : false;
		$per_page   = isset($json->per_page) ? $json->per_page : false;
		$page       = isset($json->page) ? $json->page : false;
		$filter_raw = isset($json->filter) ? $json->filter : false;
		$start      = isset($json->start) ? $json->start : false;
		$end        = isset($json->end) ? $json->end : false;

		$filter_raw = trim($filter_raw);

		if($filter_raw == 'undefined')
		{
			$filter_raw = false;
		}

		$filter     = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
		$start = (isset($start) && $start != 'null') ? $start : false;
		$end = (isset($end) && $end != 'null') ? $end : false;

		$extraWhere = array();

		$table = "payment";

		$primaryKey = "id";

		if ($start && $end)
		{
			$extraWhere[] = "DATE(payment.created) >= '$start' AND DATE(payment.created) <= '$end' ";
		}

		if ($filter)
		{
			if (strlen($filter) > 3)
			{
				$extraWhere[] = "payment.reference REGEXP '$filter' ";
			}
		}

		$extraWhere[] = "payment.client_id = 1 ";
		$extraWhere[] = "payment.status = 1 ";
		$extraWhere[] = "payment.profile_id = $id ";

		if (count($extraWhere) > 0)
		{
			$where = implode(" AND ",$extraWhere);
		}
		else
		{
			$where = 1;
		}

		$joinQuery [] = "INNER JOIN payment_type On payment.payment_type_id = payment_type.id ";

		$fields [] = "$table.$primaryKey";
		$fields [] = "payment.amount";
		$fields [] = "payment.reference";
		$fields [] = "payment.status";
		$fields [] = "payment_type.name";
		$fields [] = "DATE_FORMAT(payment.created,'%h:%i%, %d %b %y') as created";

		$groupBy = array();
		$groupBy[] = "payment.id";

		if (count($joinQuery) > 0)
		{
			$join = implode(" ",$joinQuery);
		}
		else
		{
			$join = '';
		}

		if (count($fields) > 0)
		{
			$fields = implode(",",$fields);
		}
		else
		{
			$fields = " $table.$primaryKey ";
		}

		if($sort)
		{
			list($sortByColumn,$sortBy) = explode('|',$sort);
			$orderBy = "ORDER BY $sortByColumn $sortBy";
		}
		else
		{
			$orderBy = "";
		}

		if (count($groupBy) > 0)
		{
			$group_by = "GROUP BY ".implode(" ",$groupBy);
		}
		else
		{
			$group_by = '';
		}

		$export        = $request->getQuery('export');

		$export = isset($export) ? $export : 0;

		if($export == 1)
		{
			$sql = "SELECT $fields "
				. "FROM $table $join "
				. "WHERE $where "
				. "$orderBy ";

			return $this->exportQuery($sql);
		}

		$countQuery = "SELECT COUNT(DISTINCT $table.$primaryKey) id FROM `$table` $join WHERE $where $group_by";

		try
		{
			$total = $this->rawSelect($countQuery);
		}
		catch (Exception $e)
		{
			$this->log("error", $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		$total = isset($total[0]['id']) ? $total[0]['id'] : 0;

		$last_page = $this->calculateTotalPages($total,$per_page);

		$current_page = $page - 1;

		if ($current_page)
		{

			$offset = $per_page * $current_page;
		}
		else
		{
			$current_page = 0;
			$offset       = 0;
		}

		if ($offset > $total)
		{

			$offset = $total - ($current_page * $per_page);
		}

		$from = $offset + 1;

		$current_page++;

		$left_records = $total - ($current_page * $per_page);

		$sql = "SELECT $fields "
			. "FROM $table $join "
			. "WHERE $where "
			. " $group_by "
			. "$orderBy "
			. "LIMIT $offset,$per_page";

		$next_page_url = $left_records > 0 ? "api/v1/profile/payment/$id" : null;

		$prev_page_url = ($left_records + $per_page) < $total ? "api/v1/profile/payment/$id" : null;

		try
		{
			$transactions = $this->rawSelect($sql);
		}
		catch (Exception $e)
		{
			$this->log("error, " . $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		if ($transactions)
		{
			$tableData['total']         = $total;
			$tableData['per_page']      = $per_page;
			$tableData['next_page_url'] = $next_page_url;
			$tableData['prev_page_url'] = $prev_page_url;
			$tableData['current_page']  = $current_page;
			$tableData['last_page']     = $last_page;
			$tableData['from']          = $from;
			$tableData['to']            = $offset + count($transactions);

			$tableData['data'] = $transactions;

			return $this->systemResponse($tableData,200,"Success");
		}
		else
		{
			$tableData['data'] = [];
			return $this->systemResponse($tableData,200,"Not Found");
		}

		return $this->systemResponse($tableData,421,'Not Found');
	}

	public function pledge($id)
	{
		$this->view->disable();
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$sort       = isset($json->sort) ? $json->sort : false;
		$per_page   = isset($json->per_page) ? $json->per_page : false;
		$page       = isset($json->page) ? $json->page : false;
		$filter_raw = isset($json->filter) ? $json->filter : false;
		$start      = isset($json->start) ? $json->start : false;
		$end        = isset($json->end) ? $json->end : false;

		$filter_raw = trim($filter_raw);

		if($filter_raw == 'undefined')
		{
			$filter_raw = false;
		}

		$filter     = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
		$start = (isset($start) && $start != 'null') ? $start : false;
		$end = (isset($end) && $end != 'null') ? $end : false;

		$extraWhere = array();

		$table = "pledge";

		$primaryKey = "id";

		if ($start && $end)
		{
			$extraWhere[] = "DATE(pledge.created) >= '$start' AND DATE(pledge.created) <= '$end' ";
		}

		$extraWhere[] = "pledge.profile_id = $id ";

		if (count($extraWhere) > 0)
		{
			$where = implode(" AND ",$extraWhere);
		}
		else
		{
			$where = 1;
		}

		$joinQuery [] = "INNER JOIN profile On pledge.profile_id = profile.id ";
		$joinQuery [] = "LEFT JOIN pledge_payment On pledge.id = pledge_payment.pledge_id ";
		$joinQuery [] = "LEFT JOIN payment On pledge_payment.payment_id = payment.id ";

		$fields [] = "$table.$primaryKey";
		$fields [] = "profile.msisdn";
		$fields [] = "profile.first_name";
		$fields [] = "profile.middle_name";
		$fields [] = "profile.last_name";
		$fields [] = "pledge.amount";
		$fields [] = "pledge.status";
		$fields [] = "SUM(IF(payment.status = 1,payment.amount,0)) AS reedemed";
		$fields [] = "DATE_FORMAT(pledge.created,'%h:%i%, %d %b %y') as created";

		$groupBy = array();
		$groupBy[] = "pledge.id";

		if (count($joinQuery) > 0)
		{
			$join = implode(" ",$joinQuery);
		}
		else
		{
			$join = '';
		}

		if (count($fields) > 0)
		{
			$fields = implode(",",$fields);
		}
		else
		{
			$fields = " $table.$primaryKey ";
		}

		if($sort)
		{
			list($sortByColumn,$sortBy) = explode('|',$sort);
			$orderBy = "ORDER BY $sortByColumn $sortBy";
		}
		else
		{
			$orderBy = "";
		}

		if (count($groupBy) > 0)
		{
			$group_by = "GROUP BY ".implode(" ",$groupBy);
		}
		else
		{
			$group_by = '';
		}

		$export        = $request->getQuery('export');

		$export = isset($export) ? $export : 0;

		if($export == 1)
		{
			$sql = "SELECT $fields "
				. "FROM $table $join "
				. "WHERE $where "
				. "$orderBy ";

			return $this->exportQuery($sql);
		}

		$countQuery = "SELECT COUNT(DISTINCT $table.$primaryKey) id FROM `$table` $join WHERE $where $group_by";

		try
		{
			$total = $this->rawSelect($countQuery);
		}
		catch (Exception $e)
		{
			$this->log("error", $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		$total = isset($total[0]['id']) ? $total[0]['id'] : 0;

		$last_page = $this->calculateTotalPages($total,$per_page);

		$current_page = $page - 1;

		if ($current_page)
		{

			$offset = $per_page * $current_page;
		}
		else
		{
			$current_page = 0;
			$offset       = 0;
		}

		if ($offset > $total)
		{

			$offset = $total - ($current_page * $per_page);
		}

		$from = $offset + 1;

		$current_page++;

		$left_records = $total - ($current_page * $per_page);

		$sql = "SELECT $fields "
			. "FROM $table $join "
			. "WHERE $where "
			. " $group_by "
			. "$orderBy "
			. "LIMIT $offset,$per_page";

		$next_page_url = $left_records > 0 ? "api/v1/profile/payment/$id" : null;

		$prev_page_url = ($left_records + $per_page) < $total ? "api/v1/profile/payment/$id" : null;

		try
		{
			$transactions = $this->rawSelect($sql);
		}
		catch (Exception $e)
		{
			$this->log("error, " . $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		if ($transactions)
		{
			$tableData['total']         = $total;
			$tableData['per_page']      = $per_page;
			$tableData['next_page_url'] = $next_page_url;
			$tableData['prev_page_url'] = $prev_page_url;
			$tableData['current_page']  = $current_page;
			$tableData['last_page']     = $last_page;
			$tableData['from']          = $from;
			$tableData['to']            = $offset + count($transactions);

			$tableData['data'] = $transactions;

			return $this->systemResponse($tableData,200,"Success");
		}
		else
		{
			$tableData['data'] = [];
			return $this->systemResponse($tableData,200,"Not Found");
		}

		return $this->systemResponse($tableData,421,'Not Found');
	}

}

