<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use \Firebase\JWT\JWT;

class SubCategoryController extends ControllerBase
{
    public function indexAction()
    {
        
    }

    public function create()
    {
        $jwtManager  = new JwtManager();
        $request     = new Request();
        $json        = $request->getJsonRawBody();
        $description = isset($json->description) ? $json->description : null;
        $category_id  = $json->category_id;
        $name        = $json->name;
        $thumbnail   = $json->thumbnail;
        $token       = $json->token;

        if (!$name || !$thumbnail || !$category_id)
        {
            return $this->missingData();
        }
        $tokenData = $jwtManager->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingData();
        }

        $sub_category              = new SubCategory();
        $sub_category->name        = $name;
        $sub_category->thumbnail   = $thumbnail;
        $sub_category->category_id  = $category_id;
        $sub_category->description = $description;
        $sub_category->created     = date("Y-m-d H:i:s");

        if ($sub_category->save() === false)
        {
            $errors   = array();
            $messages = $sub_category->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse('Category Created');
    }

    public function all()
    {
        $jwtManager = new JwtManager();
        $request    = new Request();
        $json       = $request->getJsonRawBody();
        $token      = $json->token;

        $tokenData = $jwtManager->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingData();
        }

        $sub_categories = SubCategory::find();

        return $this->systemResponse($sub_categories);
    }

    public function update()
    {
        $jwtManager  = new JwtManager();
        $request     = new Request();
        $json        = $request->getJsonRawBody();
        $name        = isset($json->name) ? $json->name : false;
        $description = isset($json->description) ? $json->description : false;
        $category_id = isset($json->category_id) ? $json->category_id : false;
        $sub_category_id = isset($json->sub_category_id) ? $json->sub_category_id : false;
        $thumbnail   = isset($json->thumbnail) ? $json->thumbnail : false;
        $token       = $json->token;
        $data        = array();

        if (!$sub_category_id)
        {
            return $this->missingData();
        }
        $tokenData = $jwtManager->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingData();
        }

        $sub_category = SubCategory::findFirst(array("sub_category_id = '$sub_category_id'"));

        if (!$sub_category)
        {
            return $this->notFound();
        }

        if ($name)
        {
            $sub_category->name = $name;
        }

        if ($description)
        {
            $sub_category->description = $description;
        }

        if ($thumbnail)
        {
            $sub_category->thumbnail = $thumbnail;
        }

        if ($category_id)
        {
            $sub_category->category_id = $category_id;
        }

        if ($sub_category->save() === false)
        {
            $errors   = array();
            $messages = $sub_category->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse('Sub Category Updated');
    }

    public function delete()
    {
        $jwtManager  = new JwtManager();
        $request     = new Request();
        $json        = $request->getJsonRawBody();
        $sub_category_id = isset($json->sub_category_id) ? $json->sub_category_id : false;
        $token       = $json->token;

        if (!$sub_category_id)
        {
            return $this->missingData();
        }
        $tokenData = $jwtManager->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingData();
        }

        $sub_category = SubCategory::findFirst(array("sub_category_id = '$sub_category_id'"));

        if (!$sub_category)
        {
            return $this->notFound();
        }

        if ($sub_category->delete() === false)
        {
            $errors   = array();
            $messages = $sub_category->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse('Sub Category Deleted');
    }

    private function subCategoryExists($name)
    {
        $userQuery = "SELECT * from sub_category WHERE name = '$name' ";
        $sub_category  = $this->rawSelect($userQuery);
        if ($sub_category && count($sub_category) > 0)
        {
            return $sub_category[0];
        }
        else
        {
            return false;
        }
    }

    public function table()
    {
        $this->view->disable();
        $request    = new Request();
        $token      = $request->getQuery('token');
        $sort       = $request->getQuery('sort');
        $per_page   = $request->getQuery('per_page');
        $page       = $request->getQuery('page');
        $filter_raw = $request->getQuery('filter');
        $start      = $request->getQuery('start');
        $end        = $request->getQuery('end');

        $filter_raw = trim($filter_raw);

        if ($filter_raw == 'undefined')
        {
            $filter_raw = false;
        }

        $filter = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
        $start  = (isset($start) && $start != 'null') ? $start : false;
        $end    = (isset($end) && $end != 'null') ? $end : false;

        $extraWhere = array();

        $table = "sub_category";

        $primaryKey = "sub_category_id";

        if ($start && $end)
        {
            $extraWhere[] = "DATE($table.created) >= '$start' AND DATE($table.created) <= '$end' ";
        }

        if ($filter)
        {
            if (strlen($filter) > 3)
            {
                $extraWhere[] = "sub_category.name REGEXP '$filter' OR category.name REGEXP '$filter' ";
            }
        }

        if (count($extraWhere) > 0)
        {
            $where = implode(" AND ",$extraWhere);
        }
        else
        {
            $where = 1;
        }

        $joinQuery   = array();
        $joinQuery[] = " category ON sub_category.category_id = category.category_id ";

        $fields [] = "$table.$primaryKey";
        $fields [] = "sub_category.name";
        $fields [] = "category.name as category";
        $fields [] = "sub_category.created";
        $fields [] = "sub_category.thumbnail";
        $fields [] = "sub_category.description";


        if (count($joinQuery) > 0)
        {
            $join = implode(" ",$joinQuery);
        }
        else
        {
            $join = '';
        }

        if (count($fields) > 0)
        {
            $fields = implode(",",$fields);
        }
        else
        {
            $fields = " $table.$primaryKey ";
        }

        if ($sort)
        {
            list($sortByColumn,$sortBy) = explode('|',$sort);
            $orderBy = "ORDER BY $sortByColumn $sortBy";
        }
        else
        {
            $orderBy = "";
        }
        $export = $request->getQuery('export');

        $export = isset($export) ? $export : 0;

        if ($export == 1)
        {
            $sql = "SELECT $fields "
                    . "FROM $table $join "
                    . "WHERE $where "
                    . "$orderBy ";

            return $this->exportQuery($sql);
        }

        $countQuery = "SELECT COUNT(`$table`.`$primaryKey`) id FROM `$table` $join WHERE 1";

        try
        {
            $total = $this->rawSelect($countQuery);
        }
        catch (Exception $e)
        {
            $this->logger->logMessage('error',__FUNCTION__ . "." . __LINE__,"Category: " . $e->getMessage(),0,$e->getCode());
            return $this->systemResponse("error occured",500,"Error Occured");
        }

        $total = $total[0]['id'];

        $last_page = $this->calculateTotalPages($total,$per_page);

        $current_page = $page - 1;

        if ($current_page)
        {

            $offset = $per_page * $current_page;
        }
        else
        {
            $current_page = 0;
            $offset       = 0;
        }

        if ($offset > $total)
        {

            $offset = $total - ($current_page * $per_page);
        }

        $from = $offset + 1;

        $current_page++;

        $left_records = $total - ($current_page * $per_page);

        $sql = "SELECT $fields "
                . "FROM $table $join "
                . "WHERE $where "
                . "$orderBy "
                . "LIMIT $offset,$per_page";

        $next_page_url = $left_records > 0 ? "sub-category/table" : null;

        $prev_page_url = ($left_records + $per_page) < $total ? "sub-category/table" : null;

        try
        {
            $transactions = $this->rawSelect($sql);
        }
        catch (Exception $e)
        {
            $this->logger->logMessage('error',__FUNCTION__ . "." . __LINE__,"Category: " . $e->getMessage(),0,$e->getCode());
            return $this->systemResponse("error occured",500,"Error Occured");
        }

        if ($transactions)
        {
            $tableData['total']         = $total;
            $tableData['per_page']      = $per_page;
            $tableData['next_page_url'] = $next_page_url;
            $tableData['prev_page_url'] = $prev_page_url;
            $tableData['current_page']  = $current_page;
            $tableData['last_page']     = $last_page;
            $tableData['from']          = $from;
            $tableData['to']            = $offset + count($transactions);

            $tableData['data'] = $transactions;

            return $this->systemResponse($tableData,200,"Success");
        }
        else
        {
            $tableData['data'] = [];
            return $this->systemResponse($tableData,200,"Not Found");
        }

        return $this->systemResponse($tableData,421,'Not Found');
    }

    /*
     * delete users
     */

    public function removeSubCategory()
    {
        $jwtManager  = new JwtManager();
        $request     = new Request();
        $json        = $request->getJsonRawBody();
        $sub_category_id = $json->sub_category_id;
        $token       = $json->token;

        if (!$token)
        {
            return $this->missingToken();
        }

        $tokenData = $jwtManager->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingToken();
        }

        $subCategory = SubCategory::findFirst(array("sub_category_id=:id:",
                    'bind' => array("id" => $sub_category_id)));
        if (!$subCategory)
        {
            return $this->notFound();
        }
        if ($subCategory->delete() === false)
        {
            $errors   = array();
            $messages = $subCategory->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse("Sub Category Deleted");
    }

}
