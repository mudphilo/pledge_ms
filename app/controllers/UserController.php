<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use \Firebase\JWT\JWT;

class UserController extends ControllerBase
{

    public function indexAction()
    {
        
    }

	/**
	 * creates a new user
	 */
    public function create()
    {
        $request    = new Request();
        $json       = $request->getJsonRawBody();
		$this->log('info',__FILE__.".".__FUNCTION__.' REQUEST '.json_encode($json));
		$fullname  	= isset($json->fullname) ? $json->fullname : false;
        $msisdn     = isset($json->msisdn) ? $json->msisdn : false;
        $email  	= isset($json->email) ? $json->email : false;
        $client_id  	= isset($json->client_id) ? $json->client_id : 1;
		$role_id  	= isset($json->role_id) ? $json->role_id : false;
        $code       = $this->randomCode(4,'alphanumeric');

        $msisdn = $this->formatMobileNumber($msisdn);
		
        if (!$fullname || !$msisdn || !$role_id || !$email)
        {
            return $this->missingData();
        }

        if ($this->userExists($msisdn))
        {
            return $this->missingData("$msisdn is already registered");
        }

        $user                   = new User();
        $user->msisdn           = $msisdn;
		$user->email           	= $email;
		$user->fullname        	= $fullname;
        $user->status           = 1;
        $user->created          = $this->getTime();
        $user->password         = $this->security->hash($code);

        if ($user->save() === false)
        {
            $errors   = array();
            $messages = $user->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"$code user register failed");
        }

		$this->sendSMS($msisdn,'user_register',array('name' => $fullname,'code' => $code),$client_id);

		$user_id = $user->id;
        $userRole = new UserRole();
        $userRole->role_id 	= $role_id;
        $userRole->client_id	 = $client_id;
        $userRole->user_id 	= $user_id;
		$userRole->created   = date("Y-m-d H:i:s");

		if ($userRole->save() === false)
		{
			$errors   = array();
			$messages = $user->getMessages();
			foreach ($messages as $message)
			{
				$e["message"] = $message->getMessage();
				$e["field"]   = $message->getField();
				$errors[]     = $e;
			}
			return $this->systemResponse($errors,421,"Could not create use role for the newly created user");
		}

		$token = $this->issueUserToken($user);

        return $this->systemResponse(["user_id"=>$user_id,"token"=>$token,"message"=>"account successfully created"],200,'Verification Code Send');
    }

	/**
	 * checks if user exists
	 * @param $msisdn
	 *
	 * @return bool
	 */
    public function userExists($msisdn)
    {
        $userQuery = "SELECT * from user WHERE msisdn=:msisdn";
        $user      = $this->rawSelect($userQuery,array(':msisdn'=>$msisdn));
        if ($user && count($user) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

	/**
	 * user login
	 */
	public function getPassword()
	{
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$msisdn     = isset($json->msisdn) ? $json->msisdn : false;
        $client_id  = isset($json->client_id) ? $json->client_id : 1;

		if (!$msisdn)
		{
			return $this->missingData();
		}

		$msisdn = $this->formatMobileNumber($msisdn);

		$user = User::findFirst(array("msisdn=:username: AND deleted IS NULL",
			'bind' => array("username" => $msisdn)));

		if ($user)
		{
			$types = array('alphanumeric','integer');
			$code = $this->randomCode(rand(3,5),$types[rand(0,1)]);
			$user->password = $this->security->hash($code);

			if ($user->save())
			{
				$this->sendSMS($msisdn,'user_register',array('name' => $user->fullname,'code' => $code),$client_id);
				$token = $this->issueUserToken($user);
				$data = ["token"    => $token,"message" => "Enter One Time Password sent to your number/email"];
				return $this->systemResponse($data);
			}
			else {

				return $this->systemResponse("Could not generate OTP",422,"FAILED");

			}
		}

		return $this->notFound();
	}

	/**
	 * user login
	 */
    public function login()
    {
    	$request    = new Request();
        $json       = $request->getJsonRawBody();
		$password     = isset($json->password) ? $json->password : false;
		$msisdn     = isset($json->msisdn) ? $json->msisdn : false;
		$token     = isset($json->token) ? $json->token : false;

        if (!$msisdn || !$password || !$token)
        {
            return $this->missingData("Missing MSISDN or PASSWORD or TOKEN");
        }

        $password = base64_decode($password);

        $msisdn = $this->formatMobileNumber($msisdn);

        $user = User::findFirst(array("msisdn=:username: AND deleted IS NULL",
                    'bind' => array("username" => $msisdn)));
        if ($user)
        {
        	if($this->isValidUser($token, $user)) {

				if ($this->security->checkHash($password, $user->password)) {
					$token = $this->issueToken($user);
					$data = ["token" => $token, "username" => $user->fullname, "msisdn" => $user->msisdn, "user_id" => $user->id];

					return $this->systemResponse($data);
				}
				return $this->systemResponse("Password mismatch $password", 421, "FAILED");
			}
			else {
        		return $this->invalidToken();
			}
        }

        return $this->notFound("Not found");
    }

	public function table()
	{
		$this->view->disable();
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$sort       = isset($json->sort) ? $json->sort : false;
		$per_page   = isset($json->per_page) ? $json->per_page : false;
		$page       = isset($json->page) ? $json->page : false;
		$filter_raw = isset($json->filter) ? $json->filter : false;
		$start      = isset($json->start) ? $json->start : false;
		$end        = isset($json->end) ? $json->end : false;

		$filter_raw = trim($filter_raw);

		if($filter_raw == 'undefined')
		{
			$filter_raw = false;
		}

		$filter     = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
		$start = (isset($start) && $start != 'null') ? $start : false;
		$end = (isset($end) && $end != 'null') ? $end : false;

		$extraWhere = array();

		$table = "user";

		$primaryKey = "id";

		if ($start && $end)
		{
			//$extraWhere[] = "DATE(user.created) >= '$start' AND DATE(user.created) <= '$end' ";
		}

		if ($filter)
		{
			if (strlen($filter) > 3)
			{
				//$extraWhere[] = "profiles.msisdn REGEXP '$filter' ";
			}
		}

		if (count($extraWhere) > 0)
		{
			$where = implode(" AND ",$extraWhere);
		}
		else
		{
			$where = 1;
		}

		$joinQuery [] = "INNER JOIN user_role On user.id = user_role.user_id ";
		$joinQuery [] = "INNER JOIN role On user_role.role_id = role.id ";

		$fields [] = "$table.$primaryKey";
		$fields [] = "user.msisdn";
		$fields [] = "user.fullname";
		$fields [] = "user.email";
		$fields [] = "user.status";
		$fields [] = "DATE_FORMAT(user.created,'%h:%i%, %d %b %y') as created";
		$fields [] = "user.deleted";
		$fields [] = "role.id as role_id";
		$fields [] = "role.name";

		if (count($joinQuery) > 0)
		{
			$join = implode(" ",$joinQuery);
		}
		else
		{
			$join = '';
		}

		if (count($fields) > 0)
		{
			$fields = implode(",",$fields);
		}
		else
		{
			$fields = " $table.$primaryKey ";
		}

		if($sort)
		{
			list($sortByColumn,$sortBy) = explode('|',$sort);
			$orderBy = "ORDER BY $sortByColumn $sortBy";
		}
		else
		{
			$orderBy = "";
		}
		$export        = $request->getQuery('export');

		$export = isset($export) ? $export : 0;

		if($export == 1)
		{
			$sql = "SELECT $fields "
				. "FROM $table $join "
				. "WHERE $where "
				. "$orderBy ";

			return $this->exportQuery($sql);
		}

		$countQuery = "SELECT COUNT(DISTINCT $table.$primaryKey) id FROM `$table` $join WHERE $where ";

		try
		{
			$total = $this->rawSelect($countQuery);
		}
		catch (Exception $e)
		{
			$this->log("error", $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		$total = isset($total[0]['id']) ? $total[0]['id'] : 0;

		$last_page = $this->calculateTotalPages($total,$per_page);

		$current_page = $page - 1;

		if ($current_page)
		{

			$offset = $per_page * $current_page;
		}
		else
		{
			$current_page = 0;
			$offset       = 0;
		}

		if ($offset > $total)
		{

			$offset = $total - ($current_page * $per_page);
		}

		$from = $offset + 1;

		$current_page++;

		$left_records = $total - ($current_page * $per_page);

		$sql = "SELECT $fields "
			. "FROM $table $join "
			. "WHERE $where "
			. "$orderBy "
			. "LIMIT $offset,$per_page";

		$next_page_url = $left_records > 0 ? "api/v1/user/table" : null;

		$prev_page_url = ($left_records + $per_page) < $total ? "api/v1/user/table" : null;

		try
		{
			$transactions = $this->rawSelect($sql);
			foreach ($transactions as $key => $row)
			{

				$contact = $row['msisdn'] . '<br /><span class="small">' . $row['email'] . '</span>';

				$transactions[$key]['contact'] = $contact;
			}
		}
		catch (Exception $e)
		{
			$this->log("error, " . $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		if ($transactions)
		{
			$tableData['total']         = $total;
			$tableData['per_page']      = $per_page;
			$tableData['next_page_url'] = $next_page_url;
			$tableData['prev_page_url'] = $prev_page_url;
			$tableData['current_page']  = $current_page;
			$tableData['last_page']     = $last_page;
			$tableData['from']          = $from;
			$tableData['to']            = $offset + count($transactions);

			$tableData['data'] = $transactions;

			return $this->systemResponse($tableData,200,"Success");
		}
		else
		{
			$tableData['data'] = [];
			return $this->systemResponse($tableData,200,"Not Found");
		}

		return $this->systemResponse($tableData,421,'Not Found');
	}
	/**
	 * deletes a user
	 * @param int $id
	 */
    public function delete($id)
    {
        $request    = new Request();
        $json       = $request->getJsonRawBody();
        $user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

        if (!$token || !$id)
        {
            return $this->missingData();
        }

        $user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
        if (!$user)
        {
            return $this->accessDenied();
        }

        if(!$this->isValidUser($token, $user)){
        	return $this->invalidToken();
		}

		$userToDel = User::findFirst(array("id=:id:",'bind' => array("id" => $id)));

        if(!$userToDel){
        	return $this->notFound("Cannot find user with ID $id");
		}

        $userToDel->deleted = $this->getTime();

        if ($userToDel->save() === false)
        {
            $errors   = array();

            $messages = $user->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse("User Deleted");
    }

	/**
	 * activates a user
	 *
	 * @param int $id
	 */
	public function activate($id)
	{
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$userToAct = User::findFirst(array("id=:id:",'bind' => array("id" => $id)));

		if(!$userToAct){
			return $this->notFound("Cannot find user with ID $id");
		}

		$userToAct->status = 1;

		if ($userToAct->save() === false)
		{
			$errors   = array();

			$messages = $user->getMessages();
			foreach ($messages as $message)
			{
				$e["message"] = $message->getMessage();
				$e["field"]   = $message->getField();
				$errors[]     = $e;
			}
			return $this->systemResponse($errors,421,"FAILED");
		}

		return $this->systemResponse("User activated");
	}

	/**
	 * deactivates a user
	 * @param int $id
	 */
	public function deactivate($id)
	{
		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$userToAct = User::findFirst(array("id=:id:",'bind' => array("id" => $id)));

		if(!$userToAct){
			return $this->notFound("Cannot find user with ID $id");
		}

		$userToAct->status = 0;

		if ($userToAct->save() === false)
		{
			$errors   = array();

			$messages = $user->getMessages();
			foreach ($messages as $message)
			{
				$e["message"] = $message->getMessage();
				$e["field"]   = $message->getField();
				$errors[]     = $e;
			}
			return $this->systemResponse($errors,421,"FAILED");
		}

		return $this->systemResponse("User deactivated");
	}

	public function view($id){

		$request    = new Request();
		$json       = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$userTo = User::findFirst(array("id=:id:",'bind' => array("id" => $id)));

		if(!$userTo){
			return $this->notFound("Cannot find user with ID $id");
		}

		$results = new stdClass();
		$results->user = $userTo;

		foreach ($userTo->userRole as $role){
			$name = $role->Role->name;
			$rl = new stdClass();
			$rl->name = $name;
			$rl->role_id = $role->Role->id;
			$results->role = $rl;
		}

		foreach ($userTo->Notification as $ty){
			$results->notification[] = $ty;
		}

		foreach ($userTo->UserSetting as $ty){
			$results->settings[] = $ty;
		}

		return $this->systemResponse($results,200,"User data");

	}
}
