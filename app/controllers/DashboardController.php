<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Carbon\Carbon;

class DashboardController extends ControllerBase
{

    /**
     * gets order summary
     */
    //get order history
    public function getDownloadChannelSummary()
    {
        $jwtManager = new JwtManager();
        $request    = new Request();
        $token      = $request->getQuery('token');

        if (!$token)
        {
            $json  = $request->getJsonRawBody();
            $token = $json->token;

            if (!$token)
            {
                return $this->missingToken();
            }
        }

        $tokenData = $jwtManager->verifyToken($token); //require user to have a valid token
        // $tokenData = $jwtManager->verifyToken($token,"openRequest"); // used for test

        if (!$tokenData)
        {
            return $this->accessDenied();
        }

        $query = "SELECT COUNT(download.download_id) as downloads, channel.name "
                . "FROM download "
                . "INNER JOIN media ON download.media_id=media.media_id "
                . "INNER JOIN category ON media.category_id=category.category_id "
                . "INNER JOIN channel ON category.channel_id=channel.channel_id "
                . "GROUP BY channel.channel_id";

        $results = $this->rawSelect($query);
        
        return $this->systemResponse($results,200,"Downloads summary");

    }

    /**
     * gets order summary
     */
    //get order history
    public function getDownloadSummary()
    {
        $jwtManager = new JwtManager();
        $request    = new Request();
        $token      = $request->getQuery('token');

        if (!$token)
        {
            $json  = $request->getJsonRawBody();
            $token = $json->token;

            if (!$token)
            {
                return $this->missingToken();
            }
        }

        $tokenData = $jwtManager->verifyToken($token); //require user to have a valid token
        // $tokenData = $jwtManager->verifyToken($token,"openRequest"); // used for test

        if (!$tokenData)
        {
            return $this->missingData();
        }

        $ordersQuery = "select IFNULL(count(download_id),0) as total,"
                . "CASE WHEN status = 0 THEN 'pending' "
                . "WHEN  status = 1 THEN 'processing' "
                . "WHEN  status = 2 THEN 'completed' "
                . "ELSE '' END AS downloadStatus, "
                . "status FROM `download` WHERE 1 GROUP BY status; ";

        $orders = $this->rawSelect($ordersQuery);

        $results = array();
        $object  = new stdClass();

        $object->pending    = 0;
        $object->processing = 0;
        $object->completed  = 0;

        foreach ($orders as $row)
        {
            $object->$row['downloadStatus'] = $row['total'];
        }
        
        return $this->systemResponse($object,200,"Downloads summary");
    }

    /**
     *  gets graph data
     * @param int $year
     * @param int $month
     * @return string json
     */
    public function getDownloadMonthlyGraph()
    {
        $jwtManager = new JwtManager();
        $request    = new Request();
        $token      = $request->getQuery('token');
        $json       = $request->getJsonRawBody();
        $token      = $json->token;

        $startDate = isset($json->startDate) ? $json->startDate : false;
        $endDate   = isset($json->endDate) ? $json->endDate : false;

        $startDate = ($startDate && strlen($startDate) > 5) ? $startDate : false;
        $endDate   = ($endDate && strlen($endDate) > 5 ) ? $endDate : false;

        $tokenData = $jwtManager->verifyToken($token); //require user to have a valid token
        // $tokenData = $jwtManager->verifyToken($token,"openRequest"); // used for test

        if (!$tokenData)
        {
            return $this->accessDenied();
        }

        $start = date('Y-m-d');
        $end   = date('Y-m-d');

        if (!$startDate)
        {
            $startDate = $start;
        }
        if (!$endDate)
        {
            $endDate = $end;
        }
        $start = Carbon::createFromFormat('Y-m-d',$startDate);
        $end   = Carbon::createFromFormat('Y-m-d',$endDate);

        $now              = Carbon::now();
        $isToday          = $now->diffInDays($end) == 0 ? true : false;
        //$days_difference = $start->diffInDays($end);
        $hours_difference = $start->diffInHours($end);
        $graph_type       = "hourly";
        $daysInMonth      = $start->daysInMonth;

        if ($hours_difference <= 23)
        {
            // hourly graph
            $ordersQuery = "select count(download_id) as total, " .
                    "HOUR(created) as day FROM download " .
                    "WHERE DATE(created) = '$startDate' GROUP BY day ";

            if ($isToday)
            {
                $count = $now->hour;
            }
            else
            {
                $count = 23;
            }
        }
        else if ($hours_difference >= 24 && $hours_difference <= 744)
        {
            $monthsDiff = $start->diffInMonths($end);
            if ($monthsDiff < 1)
            {
                //daily graph
                $ordersQuery = "select count(download_id) as total," .
                        "DAYOFMONTH(created) as day FROM download " .
                        "WHERE DATE(created) >= '$startDate' AND DATE(created) <= '$endDate' GROUP BY day ";

                $graph_type = "daily";
                $count      = $start->diffInDays($end);
            }
            else
            {
                //monthly graph
                $ordersQuery = "select count(download_id) as total," .
                        "MONTH(created) as day FROM download " .
                        "WHERE DATE(created) >= '$startDate' AND DATE(created) <= '$endDate' GROUP BY day ";

                $graph_type = "monthly";
                $count      = $start->diffInMonths($end);
            }
        }
        else if ($hours_difference >= 745 && $hours_difference < 8760)
        {
            //monthly graph
            $ordersQuery = "select count(download_id) as total," .
                    "MONTH(created) as day FROM download " .
                    "WHERE DATE(created) >= '$startDate' AND DATE(created) <= '$endDate' GROUP BY day ";

            $graph_type = "monthly";
            $count      = $start->diffInMonths($end);
        }
        else
        {
            // yearly graph
            $ordersQuery = "select count(download_id) as total," .
                    "YEAR(created) as day FROM download " .
                    "WHERE DATE(created) >= '$startDate' AND DATE(created) <= '$endDate' GROUP BY day ";

            $graph_type = "yearly";
            $count      = $start->diffInYears($end);
        }

        $data = $this->rawSelect($ordersQuery);

        $labels = array();

        foreach ($data as $row)
        {
            $object                       = new stdClass();
            $object->label                = $row['day'];
            $object->value                = $row['total'];
            $labels['day-' . $row['day']] = $object;
        }

        $labels = $this->fillEmptySpaces($graph_type,$labels,$count,$start);
        $values = array();

        $dataLabels = array();

        foreach ($labels as $key => $value)
        {
            array_push($dataLabels,$value->label);
            array_push($values,intval($value->value));
        }

        $startDate = $start->format('jS M Y'); //   date('jS M Y',$startDate);
        $endDate   = $end->format('jS M Y'); //date('jS M Y',$endDate);

        if ($end->diffInDays($start) == 0)
        {
            $tittle = $graph_type . " Downloads for $endDate ";
        }
        else
        {
            $tittle = $graph_type . " Downloads $startDate to $endDate ";
        }

        $results['title']  = ucwords($tittle);
        $results['key']    = $dataLabels;
        $results['value']  = $values;
        $results['xLabel'] = "Downloads ";
        $results['yLabel'] = "Number of Downloads ";

        return $this->systemResponse($results,200,"Downloads Graph");
    }

    /**
     * gets transaction data
     * 
     * @param int $year
     * @param int $month
     * @return string json
     */
    public function getTransactionMonthlyGraph()
    {
        $jwtManager = new JwtManager();
        $request    = new Request();
        $token      = $request->getQuery('token');
        $json       = $request->getJsonRawBody();
        $token      = $json->token;
        $summary    = isset($json->summary) ? $json->summary : false;
        $startDate  = isset($json->startDate) ? $json->startDate : false;
        $endDate    = isset($json->endDate) ? $json->endDate : false;

        $startDate = ($startDate && strlen($startDate) > 5) ? $startDate : false;
        $endDate   = ($endDate && strlen($endDate) > 5 ) ? $endDate : false;

        $tokenData = $jwtManager->verifyToken($token); //require user to have a valid token
        // $tokenData = $jwtManager->verifyToken($token,"openRequest"); // used for test

        if (!$tokenData)
        {
            return $this->accessDenied();
        }

        $start = date('Y-m-d');
        $end   = date('Y-m-d');

        if (!$startDate)
        {
            $startDate = $start;
        }
        if (!$endDate)
        {
            $endDate = $end;
        }

        $start = Carbon::createFromFormat('Y-m-d',$startDate);
        $end   = Carbon::createFromFormat('Y-m-d',$endDate);

        $now = Carbon::now();

        $isToday     = $now->diffInDays($end) == 0 ? true : false;
        $isThisMonth = $now->diffInMonths($end) == 0 ? true : false;
        $isThisYear  = $now->diffInYears($end) == 0 ? true : false;

        //$days_difference = $start->diffInDays($end);
        $hours_difference = $start->diffInHours($end);
        $graph_type       = "hourly";
        $daysInMonth      = $start->daysInMonth;

        if ($hours_difference <= 23)
        {
            // hourly graph
            $ordersQuery = "select SUM(receivedAmount) as total, " .
                    "HOUR(created) as day FROM `transaction` " .
                    "WHERE transactionStatus = 1 AND DATE(created) = '$startDate' GROUP BY day ";
            if ($isToday)
            {
                $count = $now->hour;
            }
            else
            {
                $count = 23;
            }
        }
        else if ($hours_difference >= 24 && $hours_difference <= 744)
        {
            $monthsDiff = $start->diffInMonths($end);
            if ($monthsDiff < 1)
            {
                //daily graph
                $ordersQuery = "select SUM(receivedAmount) as total," .
                        "DAYOFMONTH(created) as day FROM `transaction` " .
                        "WHERE transactionStatus = 1 AND DATE(created) >= '$startDate' AND DATE(created) <= '$endDate' GROUP BY day ";

                $graph_type = "daily";
                $count      = $start->diffInDays($end);
            }
            else
            {
                //monthly graph
                $ordersQuery = "select SUM(receivedAmount) as total," .
                        "MONTH(created) as day FROM `transaction` " .
                        "WHERE transactionStatus = 1 AND DATE(created) >= '$startDate' AND DATE(created) <= '$endDate' GROUP BY day ";

                $graph_type = "monthly";
                $count      = $start->diffInMonths($end);
            }
        }
        else if ($hours_difference >= 745 && $hours_difference < 8760)
        {
            //monthly graph
            $ordersQuery = "select SUM(receivedAmount) as total," .
                    "MONTH(created) as day FROM `transaction` " .
                    "WHERE transactionStatus = 1 AND DATE(created) >= '$startDate' AND DATE(created) <= '$endDate' GROUP BY day ";

            $graph_type = "monthly";
            $count      = $start->diffInMonths($end);
        }
        else
        {
            // yearly graph
            $ordersQuery = "select SUM(receivedAmount) as total," .
                    "YEAR(created) as day FROM `transaction` " .
                    "WHERE transactionStatus = 1 AND DATE(created) >= '$startDate' AND DATE(created) <= '$endDate' GROUP BY day ";

            $graph_type = "yearly";
            $count      = $start->diffInYears($end);
        }

        $data = $this->rawSelect($ordersQuery);

        $labels = array();

        foreach ($data as $row)
        {
            $object                       = new stdClass();
            $object->label                = $row['day'];
            $object->value                = $row['total'];
            $labels['day-' . $row['day']] = $object;
        }

        $labels = $this->fillEmptySpaces($graph_type,$labels,$count,$start);
        $values = array();

        $dataLabels = array();

        foreach ($labels as $key => $value)
        {
            array_push($dataLabels,$value->label);
            array_push($values,intval($value->value));
        }

        $startDateL = $start->format('jS M Y'); //   date('jS M Y',$startDate);
        $endDateL   = $end->format('jS M Y'); //date('jS M Y',$endDate);

        if ($end->diffInDays($start) == 0)
        {
            $tittle = $graph_type . " transactions for $endDateL ";
        }
        else
        {
            $tittle = $graph_type . " transactions $startDateL to $endDateL ";
        }

        $results['title']  = ucwords($tittle);;
        $results['key']    = $dataLabels;
        $results['value']  = $values;
        $results['xLabel'] = "Transactions ";
        $results['yLabel'] = " Amount (KES) ";
        if ($summary)
        {
            $query = "SELECT COUNT(o.download_id) as orders, COUNT(t.transactionId) as transactions, "
                    . "SUM(t.receivedAmount) as amount"
                    . " FROM download o "
                    . " LEFT JOIN order_transaction ot ON o.download_id = ot.download_id "
                    . " LEFT JOIN transaction t ON ot.transactionId = t.transactionId "
                    . "WHERE t.transactionStatus = 1 AND DATE(o.created) >= '$startDate' AND DATE(o.created) <= '$endDate' ";

            $summary = $this->rawSelect($query);
            if ($summary)
            {
                $results['summary'] = $summary[0];
            }
            else
            {
                $results['summary'] = array("orders" => 10,"transactions" => 10,"amount" => 10);
            }
        }

        return $this->systemResponse($results,200,"Transaction Graph");
    }

    public function transactionSummary()
    {
        $jwtManager = new JwtManager();
        $request    = new Request();
        $token      = $request->getQuery('token');
        $json       = $request->getJsonRawBody();
        $token      = $json->token;

        $startDate = isset($json->startDate) ? $json->startDate : false;
        $endDate   = isset($json->endDate) ? $json->endDate : false;

        $startDate = ($startDate && strlen($startDate) > 5) ? $startDate : false;
        $endDate   = ($endDate && strlen($endDate) > 5 ) ? $endDate : false;

        $tokenData = $jwtManager->verifyToken($token); //require user to have a valid token
        // $tokenData = $jwtManager->verifyToken($token,"openRequest"); // used for test

        if (!$tokenData)
        {
            return $this->accessDenied();
        }

        if (!$startDate)
        {
            $startDate = $start;
        }
        if (!$endDate)
        {
            $endDate = $end;
        }

        $ordersQuery = "SELECT COUNT(o.download_id) as orders, COUNT(t.transactionId) as transaction, "
                . "SUM (t.receivedAmount) as amount"
                . " FROM order o "
                . " LEFT JOIN order_transaction ot ON o.download_id = ot.download_id "
                . " LEFT JOIN transaction t ON ot.transactionId = t.transactionId "
                . "WHERE t.transactionStatus = 1 AND  DATE(o.created) >= '$startDate' AND DATE(o.created) <= '$endDate' ";

        $data = $this->rawSelect($ordersQuery);
    }

}
