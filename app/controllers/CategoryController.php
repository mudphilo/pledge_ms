<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use \Firebase\JWT\JWT;

class CategoryController extends ControllerBase
{

    public function indexAction()
    {
        
    }

    public function create()
    {
        $jwtManager  = new JwtManager();
        $request     = new Request();
        $json        = $request->getJsonRawBody();
        $description = isset($json->description) ? $json->description : null;
        $channel_id  = $json->channel_id;
        $name        = $json->name;
        $thumbnail   = $json->thumbnail;
        $token       = $json->token;

        if (!$name || !$thumbnail || !$channel_id)
        {
            return $this->missingData();
        }

        $tokenData = $jwtManager->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->accessDenied();
        }

        if ($this->categoryExists($name))
        {
            return $this->systemResponse("Category Exists",421,"FAILED");
        }

        $category              = new Category();
        $category->name        = $name;
        $category->thumbnail   = $thumbnail;
        $category->channel_id  = $channel_id;
        $category->description = $description;
        $category->created     = date("Y-m-d H:i:s");

        if ($category->save() === false)
        {
            $errors   = array();
            $messages = $category->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse('Category Created');
    }

    public function all()
    {
        $request   = new Request();
        $json      = $request->getJsonRawBody();
        $token     = $json->token;
        $device_id = $json->device_id;

        if (!$device_id)
        {
            return $this->missingData("missing device_id ");
        }

        if (!$token)
        {
            return $this->missingData("missing token ");
        }

        $device = Device::findFirst("device_id = $device_id");

        if (!$device)
        {
            return $this->missingData("device not found");
        }

        if (!$this->isValidDevice($token,$device))
        {
            return $this->accessDenied();
        }

        $categories = Category::find();

        return $this->systemResponse($categories);
    }

    public function type($channel_id)
    {
        $request   = new Request();
        $json      = $request->getJsonRawBody();
        $token     = $json->token;
        $device_id = $json->device_id;

        if (!$device_id)
        {
            return $this->missingData("missing device_id ");
        }

        if (!$token)
        {
            return $this->missingData("missing token ");
        }

        $device = Device::findFirst("device_id = $device_id");

        if (!$device)
        {
            return $this->missingData("device not found");
        }

        if (!$this->isValidDevice($token,$device))
        {
            return $this->accessDenied();
        }

        $categories = Category::find("channel_id = $channel_id");

        return $this->systemResponse($categories);
    }

    public function update()
    {
        $jwtManager  = new JwtManager();
        $request     = new Request();
        $json        = $request->getJsonRawBody();
        $name        = isset($json->name) ? $json->name : false;
        $description = isset($json->description) ? $json->description : false;
        $channel_id  = isset($json->channel_id) ? $json->channel_id : false;
        $category_id = isset($json->category_id) ? $json->category_id : false;
        $thumbnail   = isset($json->thumbnail) ? $json->thumbnail : false;
        $token       = $json->token;
        $data        = array();

        if (!$category_id)
        {
            return $this->missingData();
        }
        $tokenData = $jwtManager->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingData();
        }

        $category = Category::findFirst(array("category_id = '$category_id'"));

        if (!$category)
        {
            return $this->notFound();
        }

        if ($name)
        {
            $category->name = $name;
        }

        if ($description)
        {
            $category->description = $description;
        }

        if ($thumbnail)
        {
            $category->thumbnail = $thumbnail;
        }

        if ($channel_id)
        {
            $category->channel_id = $channel_id;
        }

        if ($category->save() === false)
        {
            $errors   = array();
            $messages = $category->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse('Category Updated');
    }

    public function delete()
    { //{fullNames, MSISDN,roleId}
        $jwtManager  = new JwtManager();
        $request     = new Request();
        $json        = $request->getJsonRawBody();
        $category_id = isset($json->category_id) ? $json->category_id : false;
        $token       = $json->token;

        if (!$category_id)
        {
            return $this->missingData();
        }
        $tokenData = $jwtManager->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingData();
        }

        $category = Category::findFirst(array("category_id = '$category_id'"));

        if (!$category)
        {
            return $this->notFound();
        }

        if ($category->delete() === false)
        {
            $errors   = array();
            $messages = $category->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse('Category Deleted');
    }

    private function categoryExists($name)
    {
        $userQuery = "SELECT * from category WHERE name = '$name' ";
        $category  = $this->rawSelect($userQuery);
        if ($category && count($category) > 0)
        {
            return $category;
        }
        else
        {
            return false;
        }
    }

    public function table()
    {
        $this->view->disable();
        $request     = new Request();
        $request     = new Request();
        $json        = $request->getJsonRawBody();
        $token       = isset($json->token) ? $json->token : false;
        $sort        = isset($json->sort) ? $json->sort : false;
        $per_page    = isset($json->per_page) ? $json->per_page : false;
        $page        = isset($json->page) ? $json->page : false;
        $filter_raw  = isset($json->filter) ? $json->filter : false;
        $channel_id = isset($json->channel_id) ? $json->channel_id : false;

        $filter_raw = trim($filter_raw);

        if ($filter_raw == 'undefined')
        {
            $filter_raw = false;
        }

        $filter = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
        
        $extraWhere = array();

        $table = "category";

        $primaryKey = "category_id";

        if ($channel_id)
        {
            $extraWhere[] = " channel.channel_id = $channel_id ";
        }

        if ($filter)
        {
            if (strlen($filter) > 3)
            {
                $extraWhere[] = "category.name REGEXP '$filter' OR channel.name REGEXP '$filter' ";
            }
        }

        if (count($extraWhere) > 0)
        {
            $where = implode(" AND ",$extraWhere);
        }
        else
        {
            $where = 1;
        }

        $joinQuery   = array();
        $joinQuery[] = " channel ON category.channel_id = channel.channel_id ";

        $fields [] = "$table.$primaryKey";
        $fields [] = "category.name";
        $fields [] = "channel.name as channel";
        $fields [] = "category.created";
        $fields [] = "category.thumbnail";
        $fields [] = "category.description";


        if (count($joinQuery) > 0)
        {
            $join = implode(" ",$joinQuery);
        }
        else
        {
            $join = '';
        }

        if (count($fields) > 0)
        {
            $fields = implode(",",$fields);
        }
        else
        {
            $fields = " $table.$primaryKey ";
        }

        if ($sort)
        {
            list($sortByColumn,$sortBy) = explode('|',$sort);
            $orderBy = "ORDER BY $sortByColumn $sortBy";
        }
        else
        {
            $orderBy = "";
        }
        $export = $request->getQuery('export');

        $export = isset($export) ? $export : 0;

        if ($export == 1)
        {
            $sql = "SELECT $fields "
                    . "FROM $table $join "
                    . "WHERE $where "
                    . "$orderBy ";

            return $this->exportQuery($sql);
        }

        $countQuery = "SELECT COUNT(`$table`.`$primaryKey`) id FROM `$table` $join WHERE 1";

        try
        {
            $total = $this->rawSelect($countQuery);
        }
        catch (Exception $e)
        {
            $this->logger->logMessage('error',__FUNCTION__ . "." . __LINE__,"Category: " . $e->getMessage(),0,$e->getCode());
            return $this->systemResponse("error occured",500,"Error Occured");
        }

        $total = $total[0]['id'];

        $last_page = $this->calculateTotalPages($total,$per_page);

        $current_page = $page - 1;

        if ($current_page)
        {

            $offset = $per_page * $current_page;
        }
        else
        {
            $current_page = 0;
            $offset       = 0;
        }

        if ($offset > $total)
        {

            $offset = $total - ($current_page * $per_page);
        }

        $from = $offset + 1;

        $current_page++;

        $left_records = $total - ($current_page * $per_page);

        $sql = "SELECT $fields "
                . "FROM $table $join "
                . "WHERE $where "
                . "$orderBy "
                . "LIMIT $offset,$per_page";

        $next_page_url = $left_records > 0 ? "category/table" : null;

        $prev_page_url = ($left_records + $per_page) < $total ? "category/table" : null;

        try
        {
            $transactions = $this->rawSelect($sql);
        }
        catch (Exception $e)
        {
            $this->logger->logMessage('error',__FUNCTION__ . "." . __LINE__,"Category: " . $e->getMessage(),0,$e->getCode());
            return $this->systemResponse("error occured",500,"Error Occured");
        }

        if ($transactions)
        {
            foreach($transactions as $key=>$value){
                $thumbnail = "http://projects.legitimate-technology.com/imovie_api_v1/public/category/";
                $transactions[$key]['thumbnail'] = $thumbnail . $value['thumbnail'];
            }
            $tableData['total']         = $total;
            $tableData['per_page']      = $per_page;
            $tableData['next_page_url'] = $next_page_url;
            $tableData['prev_page_url'] = $prev_page_url;
            $tableData['current_page']  = $current_page;
            $tableData['last_page']     = $last_page;
            $tableData['from']          = $from;
            $tableData['to']            = $offset + count($transactions);

            $tableData['data'] = $transactions;

            return $this->systemResponse($tableData,200,"Success");
        }
        else
        {
            $tableData['data'] = [];
            return $this->systemResponse($tableData,200,"Not Found");
        }

        return $this->systemResponse($tableData,421,'Not Found');
    }

    /*
     * delete users
     */

    public function removeCategory()
    {
        $jwtManager  = new JwtManager();
        $request     = new Request();
        $json        = $request->getJsonRawBody();
        $category_id = $json->category_id;
        $token       = $json->token;

        if (!$token)
        {
            return $this->missingToken();
        }

        $tokenData = $jwtManager->verifyToken($token,'openRequest');

        if (!$tokenData)
        {
            return $this->missingToken();
        }

        $category = Category::findFirst(array("category_id=:id:",
                    'bind' => array("id" => $category_id)));
        if (!$category)
        {
            return $this->notFound();
        }
        if ($category->delete() === false)
        {
            $errors   = array();
            $messages = $category->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse("Category Deleted");
    }

}
