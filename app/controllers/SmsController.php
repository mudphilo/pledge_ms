<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use \Firebase\JWT\JWT;

class SmsController extends ControllerBase
{

    public function send()
    {
        $jwtManager = new JwtManager();
        $request    = new Request();
        $json       = $request->getJsonRawBody();
        $msisdn     = $json->msisdn;
        $shortCode  = isset($json->shortCode) ? $json->shortCode : 'AFRICASTKNG';
        $message    = $json->message;
        $token      = $json->token;

        if (!$msisdn || !$message || !$token)
        {
            return $this->missingData();
        }

        $tokenData = $jwtManager->verifyToken($token,'administrator');

        if (!$tokenData)
        {
            return $this->accessDenied();
        }

        switch ($shortCode)
        {
            case 'AFRICASTKNG':
               $gateway = new AfricasTalkingGateway($this->config->AFRICASTKNG->username,$this->config->AFRICASTKNG->key);
               $status = $gateway->sendMessage($msisdn,$message);
               $sms = new Sms();
               $sms->gateway = $gateway;
               $sms->msisdn = $msisdn;
               $sms->message = $message;
               $sms->number_of_send = 1;
               $sms->status = $status;
               $sms->created = date("Y-m-d H:i:s");
               $sms->save();
                break;

            default :

                break;
        }
    }

}
