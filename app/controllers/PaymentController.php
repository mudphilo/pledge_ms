<?php

use Phalcon\Mvc\Controller;
use Phalcon\Http\Request;
use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use \Firebase\JWT\JWT;

class PaymentController extends ControllerBase
{

	/**
	 * created a payment resource
	 */
    public function create()
    {
        $request      = new Request();
        $json         = $request->getJsonRawBody();
        $amount       = $json->amount;
        $msisdn       = $json->msisdn;
        $reference    = $json->reference;
        $payment_type_id = $json->payment_type_id;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}


		if (!$msisdn || !$reference || !$amount || !$payment_type_id)
        {
            return $this->missingData();
        }

        $profile = $this->createProfile($msisdn);

        $payment               = new Payment();
        $payment->created    	= $this->getTime();
        $payment->reference    = $reference;
        $payment->profile_id 		= $profile->id;
        $payment->amount       = $amount;
        $payment->client_id = 1;
        $payment->payment_type_id = $payment_type_id;
        $payment->status = 0;

        if ($payment->save() === false)
        {
            $errors   = array();
            $messages = $payment->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
			return $this->systemResponse($errors,421,"failed to created payment");
		}

		return $this->systemResponse("payment created successfully",200,'Payment Recorded Successfully');
    }

	/**
	 * created a payment resource
	 */
	public function paymentTypes()
	{
		$request      = new Request();
		$json         = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$types = PaymentType::find();

		return $this->systemResponse($types,200,'Payment Types Retrieved Successfully');
	}

	public function view($id)
    {
        $request = new Request();
        $json    = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}


		$payment = Payment::findFirst(array("id=:id:",'bind' => array("id" => $id)));
		return $this->systemResponse($payment);
    }

    public function update($id)
    {
        $request              = new Request();
        $json                 = $request->getJsonRawBody();
		$amount     		= isset($json->amount) ? $json->amount : false;
		$status     		= isset($json->status) ? $json->status : false;
		$reference     		= isset($json->reference) ? $json->reference : false;
		$payment_type_id    = isset($json->payment_type_id) ? $json->payment_type_id : false;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}


		$payment = Payment::findFirst(array("id=:id:",'bind' => array("id" => $id)));

		if(!$payment){
			return $this->missingData("Payment not found");
		}


        if ($status)
        {
            $payment->status = $status;
        }

        if ($reference)
        {
            $payment->reference = $reference;
        }

        if ($payment_type_id)
        {
            $payment->payment_type_id = $payment_type_id;
        }

        if ($amount)
        {
            $payment->amount = $amount;
        }

        if ($payment->save() === false)
        {
            $errors   = array();
            $messages = $payment->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse($payment,200,'Payment Updated');
    }

    public function delete($id)
    {
        $request    = new Request();
        $json       = $request->getJsonRawBody();
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}


		$payment = Payment::findFirst(array("id=:id:",'bind' => array("id" => $id)));

		if(!$payment){
			return $this->missingData("Payment not found");
		}

		$payment->status = -1;

        if ($payment->save() === false)
        {
            $errors   = array();
            $messages = $payment->getMessages();
            foreach ($messages as $message)
            {
                $e["message"] = $message->getMessage();
                $e["field"]   = $message->getField();
                $errors[]     = $e;
            }
            return $this->systemResponse($errors,421,"FAILED");
        }

        return $this->systemResponse('payment Deleted');
    }

	/**
	 * gets payment vue-table
	 */
	public function table()
	{
		$this->view->disable();
		$request    = new Request();
		$json       = $request->getJsonRawBody();

		$sort       = isset($json->sort) ? $json->sort : false;
		$per_page   = isset($json->per_page) ? $json->per_page : false;
		$page       = isset($json->page) ? $json->page : false;
		$filter_raw = isset($json->filter) ? $json->filter : false;
		$start      = isset($json->start) ? $json->start : false;
		$end        = isset($json->end) ? $json->end : false;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$filter_raw = trim($filter_raw);

		if($filter_raw == 'undefined')
		{
			$filter_raw = false;
		}

		$filter     = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
		$start = (isset($start) && $start != 'null') ? $start : false;
		$end = (isset($end) && $end != 'null') ? $end : false;

		$extraWhere = array();

		$table = "payment";

		$primaryKey = "id";

		if ($start && $end)
		{
			$extraWhere[] = "DATE(payment.created) >= '$start' AND DATE(payment.created) <= '$end' ";
		}

		if ($filter)
		{
			if (strlen($filter) > 3)
			{
				$extraWhere[] = "payment.reference REGEXP '$filter' OR profile.msisdn ='$filter'";
			}
		}

		$extraWhere[] = "payment.client_id = 1 ";

		if (count($extraWhere) > 0)
		{
			$where = implode(" AND ",$extraWhere);
		}
		else
		{
			$where = 1;
		}

		$joinQuery [] = "INNER JOIN profile On payment.profile_id = profile.id ";
		$joinQuery [] = "INNER JOIN payment_type On payment.payment_type_id = payment_type.id ";

		$fields [] = "$table.$primaryKey";
		$fields [] = "profile.msisdn";
		$fields [] = "profile.first_name";
		$fields [] = "profile.middle_name";
		$fields [] = "profile.last_name";
		$fields [] = "payment.status";
		$fields [] = "payment.amount";
		$fields [] = "payment.reference";
		$fields [] = "payment_type.name";
		$fields [] = "CASE payment.status WHEN 0 THEN 'Unconfirmed' WHEN 1 THEN 'CONFIRMED' WHEN -1 THEN 'DELETED' END as paymentStatus";
		$fields [] = "DATE_FORMAT(payment.created,'%h:%i%, %d %b %y') as created";


		$groupBy = array();
		$groupBy[] = "payment.id";

		if (count($joinQuery) > 0)
		{
			$join = implode(" ",$joinQuery);
		}
		else
		{
			$join = '';
		}

		if (count($fields) > 0)
		{
			$fields = implode(",",$fields);
		}
		else
		{
			$fields = " $table.$primaryKey ";
		}

		if($sort)
		{
			list($sortByColumn,$sortBy) = explode('|',$sort);
			$orderBy = "ORDER BY $sortByColumn $sortBy";
		}
		else
		{
			$orderBy = "";
		}

		if (count($groupBy) > 0)
		{
			$group_by = "GROUP BY ".implode(" ",$groupBy);
		}
		else
		{
			$group_by = '';
		}

		$export        = $request->getQuery('export');

		$export = isset($export) ? $export : 0;

		if($export == 1)
		{
			$sql = "SELECT $fields "
				. "FROM $table $join "
				. "WHERE $where "
				. "$orderBy ";

			return $this->exportQuery($sql);
		}

		$countQuery = "SELECT COUNT(DISTINCT $table.$primaryKey) id FROM `$table` $join WHERE $where ";

		try
		{
			$total = $this->rawSelect($countQuery);
		}
		catch (Exception $e)
		{
			$this->log("error", $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		$total = isset($total[0]['id']) ? $total[0]['id'] : 0;

		$last_page = $this->calculateTotalPages($total,$per_page);

		$current_page = $page - 1;

		if ($current_page)
		{

			$offset = $per_page * $current_page;
		}
		else
		{
			$current_page = 0;
			$offset       = 0;
		}

		if ($offset > $total)
		{

			$offset = $total - ($current_page * $per_page);
		}

		$from = $offset + 1;

		$current_page++;

		$left_records = $total - ($current_page * $per_page);

		$sql = "SELECT $fields "
			. "FROM $table $join "
			. "WHERE $where "
			. " $group_by "
			. "$orderBy "
			. "LIMIT $offset,$per_page";

		$next_page_url = $left_records > 0 ? "api/v1/payment/table" : null;

		$prev_page_url = ($left_records + $per_page) < $total ? "api/v1/payment/table" : null;

		try
		{
			$transactions = $this->rawSelect($sql);
		}
		catch (Exception $e)
		{
			$this->log("error, " . $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}

		if ($transactions)
		{
			$tableData['total']         = $total;
			$tableData['per_page']      = $per_page;
			$tableData['next_page_url'] = $next_page_url;
			$tableData['prev_page_url'] = $prev_page_url;
			$tableData['current_page']  = $current_page;
			$tableData['last_page']     = $last_page;
			$tableData['from']          = $from;
			$tableData['to']            = $offset + count($transactions);

			$tableData['data'] = $transactions;

			return $this->systemResponse($tableData,200,"Success");
		}
		else
		{
			$tableData['data'] = [];
			return $this->systemResponse($tableData,200,"Not Found");
		}

		return $this->systemResponse($tableData,421,'Not Found');
	}

	/**
	 * gets payment vue-table
	 */
	public function summary()
	{
		$this->view->disable();
		$request    = new Request();
		$json       = $request->getJsonRawBody();

		$filter_raw = isset($json->filter) ? $json->filter : false;
		$start      = isset($json->start) ? $json->start : false;
		$end        = isset($json->end) ? $json->end : false;
		$user_id     = isset($json->user_id) ? $json->user_id : false;
		$token      = isset($json->token) ? $json->token : false;

		if (!$token || !$user_id)
		{
			return $this->missingData();
		}

		$user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));
		if (!$user)
		{
			return $this->accessDenied();
		}

		if(!$this->isValidUser($token, $user)){
			return $this->invalidToken();
		}

		$filter_raw = trim($filter_raw);

		if($filter_raw == 'undefined')
		{
			$filter_raw = false;
		}

		$filter     = (isset($filter_raw) && strlen($filter_raw) > 3) ? $filter_raw : false;
		$start = (isset($start) && $start != 'null') ? $start : false;
		$end = (isset($end) && $end != 'null') ? $end : false;

		$extraWhere = array();

		$table = "payment";

		$primaryKey = "id";

		if ($start && $end)
		{
			$extraWhere[] = "DATE(payment.created) >= '$start' AND DATE(payment.created) <= '$end' ";
		}

		if ($filter)
		{
			if (strlen($filter) > 3)
			{
				$extraWhere[] = "payment.reference REGEXP '$filter' OR profile.msisdn ='$filter'";
			}
		}

		$extraWhere[] = "payment.client_id = 1 ";

		if (count($extraWhere) > 0)
		{
			$where = implode(" AND ",$extraWhere);
		}
		else
		{
			$where = 1;
		}

		$joinQuery [] = "INNER JOIN profile On payment.profile_id = profile.id ";
		$joinQuery [] = "INNER JOIN payment_type On payment.payment_type_id = payment_type.id ";

		$fields [] = "COUNT(payment.id) as transaction";
		$fields [] = "SUM(payment.amount) as total";
		$fields [] = "payment_type.name";
		$fields [] = "payment_type.id";


		$groupBy = array();
		$groupBy[] = "payment_type_id";

		if (count($joinQuery) > 0)
		{
			$join = implode(" ",$joinQuery);
		}
		else
		{
			$join = '';
		}

		if (count($fields) > 0)
		{
			$fields = implode(",",$fields);
		}
		else
		{
			$fields = " $table.$primaryKey ";
		}

		if (count($groupBy) > 0)
		{
			$group_by = "GROUP BY ".implode(" ",$groupBy);
		}
		else
		{
			$group_by = '';
		}

		$sql = "SELECT $fields "
			. "FROM $table $join "
			. "WHERE $where "
			. " $group_by ";

		try
		{
			$total = $this->rawSelect($sql);
			return $this->systemResponse($total,200,"Success");
		}
		catch (Exception $e)
		{
			$this->log("error", $e->getMessage(),0,$e->getCode());
			return $this->systemResponse("error occured",500,"Error Occured");
		}
	}


}
