<?php

	use Phalcon\Http\Request;
	use Carbon\Carbon;

	class NotificationController extends ControllerBase
	{

		/**
		 * created a group resource
		 */
		public function create()
		{
			$request = new Request();
			$json = $request->getJsonRawBody();

			$recipient = isset($json->recipient) ? $json->recipient : false;
			$recipient_type_id = isset($json->recipient_type_id) ? $json->recipient_type_id : false;
			$message = isset($json->message) ? $json->message : false;
			$send_time = isset($json->send_time) ? $json->send_time : date('H:i');
			$repeat_type_id = isset($json->repeat_type_id) ? $json->repeat_type_id : false;
			$repeat_interval = isset($json->interval) ? $json->interval : false;
			$start_date = isset($json->start_date) ? $json->start_date : date('Y-m-d');
			$end_date = isset($json->end_date) ? $json->end_date : date('Y-m-d');
			$user_id = isset($json->user_id) ? $json->user_id : false;
			$token = isset($json->token) ? $json->token : false;

			if (!$token || !$user_id || !$message || !$send_time || !$repeat_type_id || !$repeat_interval || !$start_date || !$end_date) {
				return $this->missingData();
			}

			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}

			$start = Carbon::createFromFormat('Y-m-d', $start_date);
			$end = Carbon::createFromFormat('Y-m-d', $end_date);

			$diff = $end->diffInDays($start,false);

			if($diff > 1){

				return $this->missingData("Invalid dates supplied $diff");
			}

			$notification = new Notification();
			$notification->created = $this->getTime();
			$notification->client_id = 1;
			$notification->user_id = $user->id;
			$notification->end_date = $end->format('Y-m-d');
			$notification->start_date = $start->format('Y-m-d');
			$notification->status = 1;
			$notification->message = trim($message);
			$notification->repeat_interval = $repeat_interval;
			$notification->repeat_type_id = $repeat_type_id;
			$notification->send_time = $send_time;

			if ($notification->save() === false) {
				$errors = array();
				$messages = $notification->getMessages();
				foreach ($messages as $message) {
					$e["message"] = $message->getMessage();
					$e["field"] = $message->getField();
					$errors[] = $e;
				}
				return $this->systemResponse($errors, 421, "failed to created group");
			}

			$recipient = explode(",", $recipient);

			if($recipient){

				foreach ($recipient as $rec){

					if($recipient_type_id == 3){
						$recObj = $this->createProfile($this->formatMobileNumber($rec));
						$rec = $recObj->id;
					}

					$notificationRecipient = new NotificationRecipient();
					$notificationRecipient->notification_id = $notification->id;
					$notificationRecipient->recipient = $rec;
					$notificationRecipient->recipient_type_id = $recipient_type_id;
					$notificationRecipient->created = $this->getTime();

					if ($notificationRecipient->save() === false) {

						$errors = array();
						$messages = $notificationRecipient->getMessages();
						foreach ($messages as $message) {
							$e["message"] = $message->getMessage();
							$e["field"] = $message->getField();
							$errors[] = $e;
						}
						return $this->systemResponse($errors, 421, "failed to subscribe recipient to message");

					}

				}
			}

			return $this->systemResponse($notification, 200, 'Message Created Successfully');
		}

		public function view($id)
		{
			$request = new Request();
			$json = $request->getJsonRawBody();
			$user_id = isset($json->user_id) ? $json->user_id : false;
			$token = isset($json->token) ? $json->token : false;

			if (!$token || !$user_id) {
				return $this->missingData();
			}

			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}

			$filters = ["id=:id: ", "bind" => ["id" => $id]];

			$group = NotificationGroup::findFirst($filters);
			if (!$group) {
				return $this->missingData("Group does not exists");
			}

			$sql = "SELECT p.msisdn,p.first_name,p.middle_name,p.last_name FROM profile_group pg INNER JOIN profile p ON pg.profile_id=p.id INNER JOIN notification_group ng ON pg.notification_group_id=ng.id WHERE ng.id = :id";

			$params = array(":id" => $group->id);

			$data = $this->rawSelect($sql, $params);

			$response = new stdClass();
			$response->id = $group->id;
			$response->name = $group->name;
			$response->created = $group->created;
			$response->client_id = $group->client_id;
			$response->members = $data;

			return $this->systemResponse($response, 200, "SUCCESS");
		}

		public function subscribe($id)
		{
			$request = new Request();
			$json = $request->getJsonRawBody();
			$user_id = isset($json->user_id) ? $json->user_id : false;
			$recipient_type_id = isset($json->recipient_type_id) ? $json->recipient_type_id : false;
			$recipient = isset($json->recipient) ? $json->recipient : false;
			$profile_ids = isset($json->profile_id) ? $json->profile_id : false;
			$token = isset($json->token) ? $json->token : false;

			if (!$token || !$user_id) {
				return $this->missingData();
			}

			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}

			if (!$recipient_type_id) {
				return $this->missingData();
			}

			$filters = ["id=:id: ", "bind" => ["id" => $id]];

			$notification = Notification::findFirst($filters);

			if (!$notification) {
				return $this->missingData("Message does not exists");
			}

			if($recipient_type_id == 3){

				$msisdns = explode(",", trim($recipient));

				$recipient = array();
				foreach ($msisdns as $msisdn){

					$msisdn = $this->formatMobileNumber($msisdn);

					if($msisdn) {
						$profile = $this->createProfile($msisdn);
						$recipient[] = $profile->id;
					}
				}
				if($profile_ids){
					$profile_id = explode(",", $profile_ids);
					foreach ($profile_id as $ids){
						$recipient[] = $ids;
					}
				}
			}
			else if($recipient_type_id == 2){

				$recipient = explode(",", trim($recipient));
			}
			else {
				$recipient = false;
			}

			if ($recipient && !$recipient_type_id) {
				return $this->missingData("Recipient required for selected group");
			}

			if($recipient_type_id == 1){

				$check = "SELECT id FROM notification_recipient WHERE notification_id = :notification_id AND recipient_type_id = :recipient_type_id";

				$params = array(
					":notification_id" => $notification->id,
					":recipient_type_id" => $recipient_type_id
				);

				$checkRes = $this->rawSelect($check,$params);

				if($checkRes && count($checkRes) > 0){

				}
				else {
					$notificationRecipient = new NotificationRecipient();
					$notificationRecipient->notification_id = $notification->id;
					$notificationRecipient->recipient_type_id = $recipient_type_id;
					$notificationRecipient->created = $this->getTime();

					if ($notificationRecipient->save() === false) {
						$errors = array();
						$messages = $notificationRecipient->getMessages();
						foreach ($messages as $message) {
							$e["message"] = $message->getMessage();
							$e["field"] = $message->getField();
							$errors[] = $e;
						}
						return $this->systemResponse($errors, 421, "Failed to subscribe recipient to message");
					}
				}
			}
			else {

				foreach ($recipient as $datum){

					$check = "SELECT id FROM notification_recipient WHERE notification_id = :notification_id AND recipient_type_id = :recipient_type_id AND recipient = :recipient";

					$params = array(
						":notification_id" => $notification->id,
						":recipient_type_id" => $recipient_type_id,
						":recipient" => $datum
					);

					$checkRes = $this->rawSelect($check,$params);

					if($checkRes && count($checkRes) > 0){

					}
					else {

						$notificationRecipient = new NotificationRecipient();
						$notificationRecipient->notification_id = $notification->id;
						$notificationRecipient->recipient = $datum;
						$notificationRecipient->recipient_type_id = $recipient_type_id;
						$notificationRecipient->created = $this->getTime();

						if ($notificationRecipient->save() === false) {
							$errors = array();
							$messages = $notificationRecipient->getMessages();
							foreach ($messages as $message) {
								$e["message"] = $message->getMessage();
								$e["field"] = $message->getField();
								$errors[] = $e;
							}
							return $this->systemResponse($errors, 421, "Failed to subscribe recipient to message");
						}

					}

				}
			}

			return $this->systemResponse("Members subscribed to message", 200, "SUCCESS");
		}

		public function unsubscribe($id)
		{
			$request = new Request();
			$json = $request->getJsonRawBody();
			$user_id = isset($json->user_id) ? $json->user_id : false;
			$recipient_type_id = isset($json->recipient_type_id) ? $json->recipient_type_id : false;
			$recipient = isset($json->recipient) ? $json->recipient : false;
			$token = isset($json->token) ? $json->token : false;

			if (!$token || !$user_id) {
				return $this->missingData();
			}

			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}

			if (!$recipient_type_id) {
				return $this->missingData();
			}

			$filters = ["id=:id: ", "bind" => ["id" => $id]];

			$notification = Notification::findFirst($filters);

			if (!$notification) {
				return $this->missingData("Message does not exists");
			}

			if($recipient_type_id == 3){

				$msisdns = explode(",", trim($recipient));

				$recipient = array();

				foreach ($msisdns as $msisdn){

					$msisdn = $this->formatMobileNumber($msisdn);

					if($msisdn) {
						$profile = $this->createProfile($msisdn);
						$recipient[] = $profile->id;
					}
				}
			}
			else if($recipient_type_id == 2){

				$recipient = explode(",", trim($recipient));
			}
			else {
				$recipient = false;
			}

			if ($recipient && !$recipient_type_id) {
				return $this->missingData("Recipient required for selected group");
			}

			if($recipient_type_id == 1){

				$check = "DELETE FROM notification_recipient WHERE notification_id = :notification_id AND recipient_type_id = :recipient_type_id AND recipient IS NULL";

				$params = array(
					":notification_id" => $notification->id,
					":recipient_type_id" => $recipient_type_id
				);

				$this->rawInsert($check,$params);
			}
			else {

				$check = "DELETE FROM notification_recipient WHERE notification_id = :notification_id AND recipient_type_id = :recipient_type_id AND recipient IN (".implode(",", $recipient).")";

					$params = array(
						":notification_id" => $notification->id,
						":recipient_type_id" => $recipient_type_id,
					);

				$this->rawInsert($check,$params);

			}

			return $this->systemResponse("Members unsubscribed to message", 200, "SUCCESS");
		}

		public function update($id)
		{
			$request = new Request();
			$json = $request->getJsonRawBody();
			$recipient_type_id = isset($json->recipient_type_id) ? $json->recipient_type_id : false;
			$recipient = isset($json->recipient) ? $json->recipient : false;
			$message = isset($json->message) ? $json->message : false;
			$send_time = isset($json->send_time) ? $json->send_time : false;
			$repeat_type_id = isset($json->repeat_type_id) ? $json->repeat_type_id : false;
			$repeat_interval = isset($json->repeat_interval) ? $json->repeat_interval : false;
			$start_date = isset($json->start_date) ? $json->start_date : false;
			$end_date = isset($json->end_date) ? $json->end_date : false;
			$status = isset($json->status) ? $json->status : false;
			$user_id = isset($json->user_id) ? $json->user_id : false;
			$token = isset($json->token) ? $json->token : false;

			if (!$token || !$user_id ) {
				return $this->missingData();
			}

			$filters = ["id=:id:", "bind" => ["id" => $id]];

			$notification = Notification::findFirst($filters);
			if (!$notification) {
				return $this->missingData("Notification does not exists");
			}


			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}

			if($start_date)
				$start = Carbon::createFromFormat('Y-m-d', $start_date);
			else
				$start = Carbon::createFromFormat('Y-m-d', $notification->start_date);

			if($end_date)
				$end = Carbon::createFromFormat('Y-m-d', $end_date);
			else
				$end = Carbon::createFromFormat('Y-m-d', $notification->end_date);


			$diff = $end->diffInSeconds($start,false);

			if($diff < 0){

				return $this->missingData("Invalid dates supplied ");
			}

			if($start_date)
				$notification->end_date = $end->format('Y-m-d');

			if($end_date)
				$notification->start_date = $start->format('Y-m-d');

			if($status)
				$notification->status = $status;

			if($message)
				$notification->message = trim($message);

			if($repeat_interval)
				$notification->repeat_interval = $repeat_interval;

			if($repeat_type_id)
				$notification->repeat_type_id = $repeat_type_id;

			if($send_time)
				$notification->send_time = $send_time;

			if ($notification->save() === false) {
				$errors = array();
				$messages = $notification->getMessages();
				foreach ($messages as $message) {
					$e["message"] = $message->getMessage();
					$e["field"] = $message->getField();
					$errors[] = $e;
				}
				return $this->systemResponse($errors, 421, "failed to updated message");
			}

			return $this->systemResponse("message updated successfully", 200, 'Message Updated Successfully');

		}

		public function send()
		{
			$request = new Request();
			$json = $request->getJsonRawBody();
			$message = isset($json->message) ? $json->message : false;
			$msisdn = isset($json->msisdn) ? $json->msisdn : false;
			$user_id = isset($json->user_id) ? $json->user_id : false;
			$token = isset($json->token) ? $json->token : false;

			if (!$token || !$user_id ) {
				return $this->missingData();
			}

			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}

			$msisdns = explode(",", $msisdn);

			$errors = array();

			foreach ($msisdns as $msisdn) {

				$msisdn = $this->formatMobileNumber($msisdn);

				if (!$msisdn) {
					$this->log("error","Invalid MSISDN $msisdn");
					continue;
				}

				$outbox = new Outbox();
				$outbox->created = $this->getTime();
				$outbox->client_id = 1;
				$outbox->msisdn = $msisdn;
				$outbox->message = trim($message);
				$outbox->sender_id = "SOUTHWELL";

				if ($outbox->save() === false) {
					$messages = $outbox->getMessages();
					foreach ($messages as $message) {
						$e["message"] = $message->getMessage();
						$e["field"] = $message->getField();
						$errors[] = $e;
					}

					$this->log("error",json_encode($errors));
				}
			}

			return $this->systemResponse("outbox created successfully", 200, 'Message Created Successfully');

		}

		public function delete($id)
		{
			$request = new Request();
			$json = $request->getJsonRawBody();
			$user_id = isset($json->user_id) ? $json->user_id : false;
			$token = isset($json->token) ? $json->token : false;

			if (!$token || !$user_id) {
				return $this->missingData();
			}

			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}


			$notification = Notification::findFirst(array("id=:id:", 'bind' => array("id" => $id)));

			if (!$notification) {
				return $this->missingData("Notification not found");
			}

			$sql = "DELETE FROM notification_recipient WHERE notification_id = " . $notification->id;

			$this->rawInsert($sql);

			if ($notification->delete() === false) {
				$errors = array();
				$messages = $notification->getMessages();
				foreach ($messages as $message) {
					$e["message"] = $message->getMessage();
					$e["field"] = $message->getField();
					$errors[] = $e;
				}
				return $this->systemResponse($errors, 421, "FAILED");
			}

			return $this->systemResponse('Message Deleted');
		}

		/**
		 * gets payment vue-table
		 */
		public function table()
		{
			$this->view->disable();
			$request = new Request();
			$json = $request->getJsonRawBody();

			$sort = isset($json->sort) ? $json->sort : false;
			$per_page = isset($json->per_page) ? $json->per_page : false;
			$page = isset($json->page) ? $json->page : false;
			$filter_raw = isset($json->filter) ? $json->filter : false;
			$start = isset($json->start) ? $json->start : false;
			$end = isset($json->end) ? $json->end : false;
			$user_id = isset($json->user_id) ? $json->user_id : false;
			$token = isset($json->token) ? $json->token : false;

			if (!$token || !$user_id) {
				return $this->missingData();
			}

			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}

			$start = (isset($start) && $start != 'null') ? $start : false;
			$end = (isset($end) && $end != 'null') ? $end : false;

			$extraWhere = array();

			$table = "notification";

			$primaryKey = "id";

			if ($start && $end) {
				//$extraWhere[] = "DATE(notification.created) >= '$start' AND DATE(notification.created) <= '$end' ";
			}

			$extraWhere[] = "notification.client_id = 1 ";

			if (count($extraWhere) > 0) {
				$where = implode(" AND ", $extraWhere);
			} else {
				$where = 1;
			}

			$joinQuery [] = "INNER JOIN user On notification.user_id = user.id ";
			$joinQuery [] = "INNER JOIN notification_recipient On notification.id = notification_recipient.notification_id ";
			$joinQuery [] = "INNER JOIN recipient_type On notification_recipient.recipient_type_id = recipient_type.id ";
			$joinQuery [] = "INNER JOIN repeat_type On notification.repeat_type_id = repeat_type.id ";

			$fields [] = "$table.$primaryKey";
			$fields [] = "notification.message";
			$fields [] = "notification.send_time";
			$fields [] = "notification.start_date";
			$fields [] = "notification.end_date";
			$fields [] = "notification.status";
			$fields [] = "notification.repeat_interval";
			$fields [] = "notification.number_of_send";
			$fields [] = "notification.created";
			$fields [] = "repeat_type.name as repeat_type";
			$fields [] = "recipient_type.name as recipient_type";
			//$fields [] = "recipient_type.name as recipient_type";
			$fields [] = "user.fullname";
			//$fields [] = "COUNT(profile_group.profile_id) as members";

			$groupBy = array();
			$groupBy[] = "notification.id";

			if (count($joinQuery) > 0) {
				$join = implode(" ", $joinQuery);
			} else {
				$join = '';
			}

			if (count($fields) > 0) {
				$fields = implode(",", $fields);
			} else {
				$fields = " $table.$primaryKey ";
			}

			if ($sort) {
				list($sortByColumn, $sortBy) = explode('|', $sort);
				$orderBy = "ORDER BY $sortByColumn $sortBy";
			} else {
				$orderBy = "";
			}

			if (count($groupBy) > 0) {
				$group_by = "GROUP BY " . implode(" ", $groupBy);
			} else {
				$group_by = '';
			}

			$export = $request->getQuery('export');

			$export = isset($export) ? $export : 0;

			if ($export == 1) {
				$sql = "SELECT $fields " . "FROM $table $join " . "WHERE $where " . "$orderBy ";

				return $this->exportQuery($sql);
			}

			$countQuery = "SELECT COUNT(DISTINCT $table.$primaryKey) id FROM `$table` $join WHERE $where $group_by";

			try {
				$total = $this->rawSelect($countQuery);
			}
			catch (Exception $e) {
				$this->log("error", $e->getMessage(), 0, $e->getCode());
				return $this->systemResponse("error occured", 500, "Error Occured");
			}

			$total = isset($total[0]['id']) ? $total[0]['id'] : 0;

			$last_page = $this->calculateTotalPages($total, $per_page);

			$current_page = $page - 1;

			if ($current_page) {

				$offset = $per_page * $current_page;
			} else {
				$current_page = 0;
				$offset = 0;
			}

			if ($offset > $total) {

				$offset = $total - ($current_page * $per_page);
			}

			$from = $offset + 1;

			$current_page++;

			$left_records = $total - ($current_page * $per_page);

			$sql = "SELECT $fields " . "FROM $table $join " . "WHERE $where " . " $group_by " . "$orderBy " . "LIMIT $offset,$per_page";

			$next_page_url = $left_records > 0 ? "api/v1/group/table" : null;

			$prev_page_url = ($left_records + $per_page) < $total ? "api/v1/group/table" : null;

			try {
				$transactions = $this->rawSelect($sql);
			}
			catch (Exception $e) {
				$this->log("error, " . $e->getMessage(), 0, $e->getCode());
				return $this->systemResponse("error occured", 500, "Error Occured");
			}


			foreach ($transactions as $key=>$value){

				$sql = "SELECT recipient_type.id as id,recipient_type.name,notification_recipient.recipient FROM notification_recipient INNER JOIN recipient_type ON notification_recipient.recipient_type_id = recipient_type.id WHERE notification_recipient.notification_id = :notification_id ";

				$data = $this->rawSelect($sql,array(":notification_id"=>$value['id']));

				foreach ($data as $dt){

					$totalMembers = array();

					if($dt['id'] == 1){

						$statement = "SELECT COUNT(id) as total,'ALL' as name FROM profile_setting WHERE client_id = 1";
						$res = $this->rawSelect($statement);
						if($res){
							$totalMembers = $res[0];
						}
					}
					else if($dt['id'] == 2){

						$statement = "SELECT COUNT(pg.id) as total,ng.name FROM profile_group pg INNER JOIN notification_group ng ON pg.notification_group_id = ng.id WHERE pg.notification_group_id = :notification_group_id";

						$res = $this->rawSelect($statement,array(":notification_group_id"=>$dt['recipient']));
						if($res){
							$totalMembers = $res[0];
						}
					}
					else if($dt['id'] == 3){

						$statement = "SELECT COUNT(id) as total,msisdn as name FROM profile WHERE id = :id";
						$res = $this->rawSelect($statement,array(":id"=>$dt['recipient']));
						if($res){
							$totalMembers = $res[0];
						}
					}

					$dt['members'] = $totalMembers['total'];
					$dt['group_name'] = $totalMembers['name'];
					$value['groups'][] = $dt;
				}


				$transactions[$key] = $value;

			}


			if ($transactions) {

				$tableData['total'] = $total;
				$tableData['per_page'] = $per_page;
				$tableData['next_page_url'] = $next_page_url;
				$tableData['prev_page_url'] = $prev_page_url;
				$tableData['current_page'] = $current_page;
				$tableData['last_page'] = $last_page;
				$tableData['from'] = $from;
				$tableData['to'] = $offset + count($transactions);

				$tableData['data'] = $transactions;

				return $this->systemResponse($tableData, 200, "Success");
			} else {
				$tableData['data'] = [];
				return $this->systemResponse($tableData, 200, "Not Found");
			}

			return $this->systemResponse($tableData, 421, 'Not Found');
		}

		public function getRepeatTypes()
		{
			$request = new Request();
			$json = $request->getJsonRawBody();
			$user_id = isset($json->user_id) ? $json->user_id : false;
			$token = isset($json->token) ? $json->token : false;

			if (!$token || !$user_id ) {
				return $this->missingData();
			}

			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}

			$data = RepeatType::find();

			return $this->systemResponse($data, 200, 'SUCCESS');

		}

		public function recipientTypes()
		{
			$request = new Request();
			$json = $request->getJsonRawBody();
			$user_id = isset($json->user_id) ? $json->user_id : false;
			$token = isset($json->token) ? $json->token : false;

			if (!$token || !$user_id ) {
				return $this->missingData();
			}

			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}

			$data = RecipientType::find();

			return $this->systemResponse($data, 200, 'SUCCESS');

		}

		public function customSend1(){

			$this->log('info',"HAPO");

			$sql = "select x.msisdn,x.first_name,x.pledged,x.paid from (select profile.msisdn,profile.first_name,pledge.profile_id,pledge.amount as pledged,sum(payment.amount) as paid from pledge inner join profile on pledge.profile_id=profile.id left join pledge_payment on pledge.id= pledge_payment.pledge_id left join payment on pledge_payment.payment_id=payment.id group by profile.msisdn)x where x.pledged >= x.paid";

			$data = $this->rawSelect($sql);

			$this->log('info',"HAPO done ");

			$this->sendMessage("254726120256", "Start sending SMS");
			$this->sendMessage("254715080766", "Start sending SMS");

			$n = 0;

			foreach ($data as $row){

				$n++;

				$first_name = $row['first_name'];
				$msisdn = $row['msisdn'];

				$message = "";

				$this->sendMessage($msisdn, $message);

			}

			$message = "SMS send to $n members who have fully redeemed their pledges";

			$this->sendMessage("254726120256", $message);
			$this->sendMessage("254715080766", $message);

		}

		public function customSend2(){

			$this->log('info',"HAPO");

			$sql = "select x.msisdn,x.first_name,x.pledged,x.paid from (select profile.msisdn,profile.first_name,pledge.profile_id,pledge.amount as pledged,sum(payment.amount) as paid from pledge inner join profile on pledge.profile_id=profile.id left join pledge_payment on pledge.id= pledge_payment.pledge_id left join payment on pledge_payment.payment_id=payment.id group by profile.msisdn)x having x.paid >0 and x.paid < x.pledged";

			$data = $this->rawSelect($sql);

			$this->log('info',"HAPO done ");

			$this->sendMessage("254726120256", "Start sending SMS");
			$this->sendMessage("254715080766", "Start sending SMS");

			$n = 0;

			foreach ($data as $row){

				$n++;

				$first_name = $row['first_name'];
				$msisdn = $row['msisdn'];

				$message = "Dear $first_name, we are sorry for the previous message, we take this opportunity to thank you for your support so far towards our wedding plans. We pray that you complete the payment of your pledge as soon as the Lord enables you to help us plan appropriately. You can pay your pledge through the paybill 587357 or through the treasurer Martin Kahara 0720080222. You can still support us through prayers and by participation in othe ongoing online 100bob campaigns or in any other way as the Lord enables you. God bless you. Anne and Harry";

				$this->sendMessage($msisdn, $message);

			}

			$message = "SMS send to $n members who have partially redeemed their pledges";

			$this->sendMessage("254726120256", $message);
			$this->sendMessage("254715080766", $message);

		}

		public function customSend4(){

			// pledge but not paid

			$this->log('info',"HAPO");

			$sql = "select x.msisdn,x.first_name,x.pledged,x.paid from (select profile.msisdn,profile.first_name,pledge.profile_id,pledge.amount as pledged,sum(payment.amount) as paid from pledge inner join profile on pledge.profile_id=profile.id left join pledge_payment on pledge.id= pledge_payment.pledge_id left join payment on pledge_payment.payment_id=payment.id group by profile.msisdn)x where x.paid = 0 OR x.paid is NULL ";

			$data = $this->rawSelect($sql);

			$this->log('info',"HAPO done ");

			$this->sendMessage("254726120256", "Start sending SMS");
			$this->sendMessage("254715080766", "Start sending SMS");

			$n = 0;

			foreach ($data as $row){

				$n++;

				$first_name = $row['first_name'];
				$msisdn = $row['msisdn'];

				$message = "Dear $first_name, we are sorry for the previous message, we take this opportunity to thank you for your support so far towards our wedding plans. We pray that you pay your pledge as soon as the Lord enables you to help us plan appropriately. You can pay your pledge through the paybill 587357 or through the treasurer Martin Kahara 0720080222. You can still support us through prayers and by participation in othe ongoing online 100bob campaigns or in any other way as the Lord enables you. God bless you. Anne and Harry";

				$this->sendMessage($msisdn, $message);

			}

			$message = "SMS send to $n members who have partially redeemed their pledges";

			$this->sendMessage("254726120256", $message);
			$this->sendMessage("254715080766", $message);

		}

		public function customSend3(){

			$this->log('info',"HAPO");

			$sql = "select msisdn,first_name from profile where id not in (select profile_id from pledge)";

			$data = $this->rawSelect($sql);

			$this->log('info',"HAPO done ");

			$this->sendMessage("254726120256", "Start sending SMS");

			$n = 0;

			foreach ($data as $row){

				$n++;

				$first_name = $row['first_name'];
				$msisdn = $row['msisdn'];

				$message = "Dear $first_name, we take this opportunity to thank you for your support so far. We are asking you to submit your pledge  as  the Lord enables you to help us plan appropriately. You can submit your pledge through the groom bro. Harry on  0715080766. God bless you abundantly. Anne and Harry";

				$this->sendMessage($msisdn, $message);

			}

			$message = "SMS send to $n members who have pledged but not paid anything";

			$this->sendMessage("254726120256", $message);
			$this->sendMessage("254715080766", $message);

		}

		public function upload() {

			$hasfiles = $this->request->hasFiles();
			$message = $this->request->getPost("message");
			$user_id = $this->request->getPost("user_id");
			$token = $this->request->getPost("token");

			if (!$token || !$user_id || !$message || !$hasfiles) {
				return $this->missingData();
			}

			$user = User::findFirst(array("id=:id:", 'bind' => array("id" => $user_id)));
			if (!$user) {
				return $this->accessDenied();
			}

			if (!$this->isValidUser($token, $user)) {
				return $this->invalidToken();
			}

			$config = include APP_PATH . "/app/config/config.php";

			$uploadDir = $config->fileUpload->directory;

			$files = $this->request->getUploadedFiles();

			foreach ($files as $key => $file) {

				$destination = $uploadDir . $key. "_" . time();
				$file->moveTo($destination);

				$recipients = exec('perl -pe \'s/\r\n|\n|\r/\n/g\' ' . escapeshellarg($destination) . ' | wc -l');

				$sql = "INSERT INTO file_uploads(client_id,message,file,status,recipients,created) value(1,:message,:file,0,:recipients,now())";

				$params = array(":message" => $message, ":file" => $destination,":recipients"=>$recipients);

				$this->rawInsert($sql, $params);

			}

			return $this->systemResponse("upload created successfully", 200, 'Message Created Successfully');
		}

        /**
         * created a payment resource
         */
        public function singleSMS()
        {
            $request      = new Request();
            $json         = $request->getJsonRawBody();
            $post         = $request->getPost();

            $this->log("info",__FUNCTION__." GOT JSON ".json_encode($json));
            $this->log("info",__FUNCTION__." GOT POST ".json_encode($request->getPost()));

            if ($json->token)
            {
                $token = $json->token;
            }
            else {

                $token = isset($post['token']) ? $post['token'] : false;
            }

            if ($json->msisdn)
            {
                $msisdn = $json->msisdn;
            }
            else {

                $msisdn = isset($post['msisdn']) ? $post['msisdn'] : false;
            }

            if ($json->user_id)
            {
                $user_id = $json->user_id;
            }
            else {

                $user_id = isset($post['user_id']) ? $post['user_id'] : false;
            }


            if ($json->message)
            {
                $message = $json->message;
            }
            else {

                $message = isset($post['message']) ? $post['message'] : false;
            }

            if (!$token)
            {
                return $this->missingData("missing token field ");
            }

            if (!$user_id)
            {
                return $this->missingData(" missing user_id field");
            }

            if (!$message)
            {
                return $this->missingData(" missing message field");
            }

            $msisdn = $this->formatMobileNumber($msisdn);

            $user = User::findFirst(array("id=:id:",'bind' => array("id" => $user_id)));

            if (!$user)
            {
                return $this->accessDenied("invalid user_id $user_id ");
            }

            if(!$this->isValidUser($token, $user)){
                //return $this->invalidToken();
            }


            if (!$msisdn || strlen($msisdn) < 12 )
            {
                return $this->missingData("missing or invalid msisdn field ");
            }

            $userRole = UserRole::findFirst(array("user_id=:user_id:",'bind' => array("user_id" => $user_id )));

            $client_id = $userRole->client_id;

            if(!$client_id){

                return $this->missingData("user does not belong to any client");

            }

            $response = $this->sendMessage($msisdn,$message,$client_id);
            //$response = $this->sendMessageSouthwell($msisdn,$message,$client_id);

            $res = intval($response['status']) == 200 ? "success" : "error occured";

            return $this->systemResponse($res,$response['status']);
        }
	}
