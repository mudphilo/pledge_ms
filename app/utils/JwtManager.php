<?php

use Phalcon\Mvc\Model\Query;
use Phalcon\Mvc\Model\Query\Builder as Builder;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;
use \Firebase\JWT\JWT;

/**
 * 
 */
class JwtManager
{

    public function verifyToken($token,$action = 0)
    {
        $key = "IMw2c3W5KWLFN1sBH1befeFocdWUs0Sd";

        $decoded;

        try
        {
            $decoded = JWT::decode($token,$key,array('HS256'));
        }
        catch (Exception $e)
        {
            $logPathLocation = $this->config->logPath->location;

            $logger = new FileAdapter($logPathLocation . 'error_logs.log');
            $logger->log("token errors " . $e->getMessage());
            $res    = new SystemResponses();
            $res->composePushLog("Exeption",$e->getMessage(),"Token decode error");
            return $decoded;
        }
        if ($decoded->action == $action)
        {
            return $decoded;
        }
        if (isset($decoded->name) && isset($decoded->userId))
        {
            return $decoded;
        }

        return;
    }

    public function issueToken($user)
    {
        $key = "IMw2c3W5KWLFN1sBH1befeFocdWUs0Sd";

        $tokenId    = base64_encode(mcrypt_create_iv(32));
        $issuedAt   = time();
        $notBefore  = $issuedAt + 10;             //Adding 10 seconds
        $expire     = $notBefore + 60;            // Adding 60 seconds
        $serverName = gethostname(); // Retrieve the server name from config file

        $token = array(
            "iss"    => $serverName,
            "iat"    => $issuedAt,
            "nbf"    => $notBefore,
            "msisdn"   => $user->msisdn,
            "user_id" => $user->user_id
        );
        $jwt   = JWT::encode($token,$key);
        return $jwt;
    }

}
