<?php

use Phalcon\Mvc\Micro;
use Phalcon\Loader;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Http\Response;
use Phalcon\Mvc\Micro\Collection as MicroCollection;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\Http\Request;

error_reporting(E_ALL);

define('APP_PATH',realpath(''));

/**
 * Read the configuration
 */
$config = include APP_PATH . "/app/config/config.php";

/**
 * Read auto-loader
 */
include APP_PATH . "/app/config/loader.php";

/**
 * Read services
 */
include APP_PATH . "/app/config/services.php";

/**
 * Read composer libraries
 */
include APP_PATH . "/vendor/autoload.php";

//create and bind the DI to the application 
$app = new Micro($di);

$user_route = new MicroCollection();
$user_route->setPrefix('/v1/user/');
$user_route->setHandler('UserController',true);
$user_route->post('login','login');
$user_route->post('create','create');
$user_route->post('table','table');
$user_route->post('{id}/delete','delete');
$user_route->post('{id}/activate','activate');
$user_route->post('{id}/deactivate','deactivate');
$user_route->post('{id}/view','view');
$user_route->post('otp','getPassword');

$profile_route = new MicroCollection();
$profile_route->setPrefix('/v1/profile/');
$profile_route->setHandler('ProfileController',true);
$profile_route->post('create','create');
$profile_route->post('table','table');
$profile_route->post('{id}/payment','payment');
$profile_route->post('{id}/pledge','pledge');

$pledge_route = new MicroCollection();
$pledge_route->setPrefix('/v1/pledge/');
$pledge_route->setHandler('PledgeController',true);
$pledge_route->post('create','create');
$pledge_route->post('{id}/update','update');
$pledge_route->post('{id}/pay','pay');
$pledge_route->post('{id}/view','view');
$pledge_route->post('table','table');
$pledge_route->post('summary','summary');

$payment_route = new MicroCollection();
$payment_route->setPrefix('/v1/payment/');
$payment_route->setHandler('PaymentController',true);
$payment_route->post('create','create');
$payment_route->post('{id}/update','update');
$payment_route->post('{id}/delete','delete');
$payment_route->post('{id}/view','view');
$payment_route->post('table','table');
$payment_route->post('summary','summary');
$payment_route->post('types','paymentTypes');


$group_route = new MicroCollection();
$group_route->setPrefix('/v1/group/');
$group_route->setHandler('GroupController',true);
$group_route->post('create','create');
$group_route->post('{id}/update','update');
$group_route->post('{id}/delete','delete');
$group_route->post('{id}/view','view');
$group_route->post('{id}/join','join');
$group_route->post('{id}/leave','leave');
$group_route->post('table','table');
$group_route->post('all','all');
$group_route->post('{id}/members','members');

$message_route = new MicroCollection();
$message_route->setPrefix('/v1/message/');
$message_route->setHandler('NotificationController',true);
$message_route->post('create','create');
$message_route->post('{id}/update','update');
$message_route->post('{id}/delete','delete');
$message_route->post('{id}/view','view');
$message_route->post('{id}/subscribe','subscribe');
$message_route->post('{id}/unsubscribe','unsubscribe');
$message_route->post('send','send');
$message_route->post('table','table');
$message_route->post('type/repeat','getRepeatTypes');
$message_route->post('type/recipient','recipientTypes');
$message_route->post('custom/send1','customSend1');
$message_route->post('custom/send2','customSend2');
$message_route->post('custom/send3','customSend3');
$message_route->post('custom/send4','customSend4');
$message_route->post('upload','upload');
$message_route->post('singleSMS','singleSMS');

$app->mount($user_route);
$app->mount($profile_route);
$app->mount($pledge_route);
$app->mount($payment_route);
$app->mount($group_route);
$app->mount($message_route);

try
{
    // Handle the request
    $response = $app->handle();
}
catch (\Exception $e)
{
    $errorLogger = new FileAdapter(LOG_PATH . '/' . ERROR_FILE);
    $errorLogger->error('BOOSTRAP '.date("Y-m-d H:i:s")."  ". $e->getMessage());

    $res = new \stdClass();
    $res->code = "Error";
    $res->message = "(ง'̀-'́)ง I wanna go home! Get me out of here please!! ༼ つ ಥ_ಥ ༽つ Am lost this page is not available ";
    $res->data = [];

    header("Content-Type: text/plain;charset=utf-8");
    header('Access-Control-Allow-Origin:*');

    $phpSapiName = substr(php_sapi_name(), 0, 3);

    if ($phpSapiName == 'cgi' || $phpSapiName == 'fpm') {

        http_response_code($e->getCode() == 400 ? 400 : 404);

    } else {

        $protocol = isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0    ';
        http_response_code($e->getCode() == 400 ? 400 : 404);

    }

    echo json_encode($res);
}
